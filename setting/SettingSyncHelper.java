package com.utils.setting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.utils.App;
import com.utils.Device;

import android.content.pm.PackageManager;

@SuppressWarnings("deprecation")
public class SettingSyncHelper
{

	public static String SERVER = null;
	public static Integer VERSION = 2;

	static public String getUser()
	{

		if(checkPermission())
		{
			String name = Device.getUsername();

			if(name != null)
			{
				return name;
			}
		}

		return Device.ID();
	}

	static boolean checkPermission()
	{
		String permission = "android.permission.GET_ACCOUNTS";
		int res = App.get().checkCallingOrSelfPermission(permission);
		return (res == PackageManager.PERMISSION_GRANTED);
	}

	static String getAdress()
	{

		if(SERVER != null)
		{
			return SERVER;
		}

		if(VERSION == 1)
		{
			int i = Math.abs(getUser().hashCode()) % 3;

			if(i == 1)
			{
				return "http://bananatwo.parseapp.com/";
			} else if(i == 2)
			{
				return "http://banana3.parseapp.com/";
			} else
			{
				return "http://banana.parseapp.com/";
			}
		} else
		{

			int i = Math.abs(getUser().hashCode()) % 9;

			if(i == 1)
			{
				return "http://bananatwo.parseapp.com/";
			} else if(i == 0)
			{
				return "http://banana.parseapp.com/";
			} else
			{
				return "http://banana" + (i + 1) + ".parseapp.com/";
			}
		}

	}

	public static boolean Send(Integer setting, String value)
	{

		HashMap<String, String> map = new HashMap<String, String>();

		map.put("user", getUser());

		String app = App.get().getPackageName();

		while (app.contains("."))
		{
			app = app.replace('.', '_');
		}

		map.put("app", app);

		map.put("id", "" + setting);
		map.put("value", value);

		return Post(getAdress() + "send", map) != null;

	}

	public static String Get(Integer setting)
	{

		HashMap<String, String> map = new HashMap<String, String>();

		map.put("user", getUser());

		String app = App.get().getPackageName();

		while (app.contains("."))
		{
			app = app.replace('.', '_');
		}

		map.put("app", app);

		map.put("id", "" + setting);

		String str = Post(getAdress() + "get_new", map);

		return str;
	}

	@SuppressWarnings({ "resource" })
	public static String Post(String url, Map<String, String> map)
	{
		if(url == null || map == null)
		{
			return null;
		}

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);

		try
		{
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			for (java.util.Map.Entry<String, String> entry : map.entrySet())
			{
				nameValuePairs.add(new BasicNameValuePair(entry.getKey(),
						entry.getValue()));
			}
			httppost.setEntity(
					new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

			
			HttpResponse response = httpclient.execute(httppost);

			String result = EntityUtils.toString(response.getEntity());
			
			return result;

		} catch (Exception e)
		{

			e.printStackTrace();
			return null;
		}

	}
}
