package com.utils.setting;

import java.io.Serializable;
import com.utils.App;
import com.utils.ThreadTransanction;
import com.utils.ThreadTransanction.ThreadRunnable;
import com.utils.ThreadTransanction.ThreadStatus;
import com.utils.WeakHandler;
import com.utils.io.Base64;
import com.utils.io.IO;
import com.utils.shorts.E;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

public class SettingHelper
{

	final static String SETTINGS = "settings";

	static SharedPreferences getSP()
	{
		return App.get().getSharedPreferences(SETTINGS, 0);
	}

	static SharedPreferences.Editor getEditor()
	{
		return getSP().edit();
	}
	
	

	public static class Storage<D extends Serializable>
	{

		protected Integer setting;
		protected D def;
		protected D value;
		protected ClassLoader loader = null;
		protected Long last_saving = 0L;
		protected String name = null;
		protected String filename = null;
		protected boolean isNeedRead = true;
		
		protected boolean isNoSavingStorage = false;
		
		public Integer ID()
		{
			return setting;
		}

		public Storage(Integer setting)
		{
			this.setting = setting;
			this.def = null;
		}
		
		public Storage(Integer setting, D def)
		{
			this.setting = setting;
			this.def = def;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public Storage(Integer setting, Class cl)
		{
			this.setting = setting;

			try
			{
				this.def = (D) cl.newInstance();
				loader = cl.getClassLoader();

			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		protected int options = Base64.NO_OPTIONS;
		public void setGZIP(boolean is_gzip)
		{
			if(is_gzip)
			{
				options = Base64.GZIP;
			}
			else
			{
				options = Base64.NO_OPTIONS;
			}
		}
		
		public void SetNoStoring(boolean isNoSavingStorage)
		{
			this.isNoSavingStorage = isNoSavingStorage;
		}
		
		
		public String getFileSettingName() {
			if(filename == null) {
				filename = String.valueOf(setting);
			}
			
			return filename;
		}
		
		public String getSettingName() {
			
			if(name == null)
			{
				if(E.Names().containsKey(setting))
				{
					name = E.Names().get(setting);
				}
				else
				{
					return String.valueOf(setting);
				}
			}
			
			return name;
		}
		
		
		public void requestChangeEvent() {
			E.Event(setting, getSettingName(), value);
		}
		
		public void Save()
		{
			Save(Get());
		}
		
		public void Save(D val) {
			Save(val, false, true);
		}
		
		public void SaveWithoutEvent(D val) {
			Save(val,false, false);
		}
		
		public void SaveWithoutStoring()
		{
			Save(value, true, true);
		}
		
		public void SaveWithoutStoring(D val)
		{
			Save(val, true, true);
		}
		
		public void Save(D val, boolean only_set) {
			Save(val, only_set, true);
		}
		
		public void Save(D val, boolean only_set, boolean aWithEvent)
		{


				this.value = val;
				isNeedRead = false;

				if(aWithEvent)
				{
					requestChangeEvent();
				}

				last_saving = System.currentTimeMillis();
				
				if(!isNoSavingStorage && !only_set)
				{

					final Long current_saving = last_saving;
					
					ThreadTransanction.execute("Save setting " + getSettingName(),
							new ThreadRunnable<Object>()
							{

								@Override
								public void result(ThreadStatus aStatus,
										Object aResult)
								{
									
									
								}

								@Override
								public Object run()
								{
									
									
									if(!current_saving.equals(last_saving))
									{
										return null;
									}
									
									try
									{
										
										if(value == null) {
											IO.RemoveFile(SETTINGS, getFileSettingName());
											//Log.w(SETTINGS, "Clear "+getSettingName());
										}
										else {
											IO.StringToFile(SETTINGS, getFileSettingName(), Base64.encodeObject(value,options));
											//Log.w(SETTINGS, "Save "+getSettingName()+" "+value);
										}

									}
									catch (Exception e)
									{
										Log.w(SETTINGS, "error "+getSettingName()+" "+value);
										
										e.printStackTrace();
									}

									return null;
								}
					});
				}
			
		}
		
		public static final Integer DEFAULT_DELAY = 5000;
		protected WeakHandler handler;
		protected boolean is_saving_handler = false;
		
		
		public void SaveDelayed()
		{
			SaveDelayed(value, DEFAULT_DELAY);
		}
		
		public void SaveDelayed(int delay)
		{
			SaveDelayed(value, delay);
		}
		
		public void SaveDelayed(D v)
		{
			SaveDelayed(v, DEFAULT_DELAY);
		}
		
		private Object lockSaveDelayed = 1;
		public void SaveDelayed(D v, int delay)
		{
			
			value = v;
			
			if(handler == null)
			{
				handler = new WeakHandler(Looper.getMainLooper());
			}

			synchronized (lockSaveDelayed)
			{
				if(!is_saving_handler) {
					
					is_saving_handler = true;

					handler.postDelayed(new Runnable()
					{

						@Override
						public void run()
						{
							try {
								Save();
							} 
							catch (Exception e) {
								e.printStackTrace();
							}

							is_saving_handler = false;
						}
						
					}, delay);
				}
			}
			

		}

		public boolean hasCache()
		{
			return value != null;
		}

		@SuppressWarnings("unchecked")
		public D Get()
		{
			if(value != null || !isNeedRead)
				return value;
			
			value = def;
			
			try
			{

				long start = System.currentTimeMillis();
				String str_messages = null;
				
				str_messages = IO.StringFromFile(SETTINGS, getFileSettingName());
				
				
				
				if(str_messages == null)
				{
					value = def;
				}
				else
				{

					value = (D) Base64.decodeToObject(str_messages,options, loader);
					
					if(def!=null && def.getClass()!=value.getClass())
					{
						value = null;
					}
					else
					{

					}
				}
				
				
				isNeedRead = false;
				
				Log.w(SETTINGS, "Get "
						+ getSettingName()
						+ " ("
						+ ((double) (System.currentTimeMillis() - start) * 0.001)
						+ "sec) ");
			

			} catch (Exception e)
			{
				
				Clear();
				
				e.printStackTrace();
			}
			
			if(value==null)
				value = def;
			
			return value;
		}

		public void ClearCache()
		{
			value = null;
			isNeedRead = true;
		}
		
		public void Clear()
		{
			Save(null, false, true);
		}

		public boolean IsEmpty()
		{
			
			return Get() == null;
		}

	}

	public static class StorageSync<D extends Serializable> extends Storage<D> {

		
		boolean is_sync = false;
		
		protected D merge(D objLocal, D objSynced)
		{
			return objSynced;
		}
		
		@Override
		public void Save(D val)
		{
			
			
			super.Save(val);
			
			
			ThreadTransanction.execute_http("sync save "+getSettingName(), new ThreadRunnable<D>()
					{
						
						@Override
						public D run()
						{
							
							if(value != null)
							{
								String str;
								
								try
								{
									str = Base64.encodeObject(value,options);
									
									Editor edit = getEditor();
									edit.putBoolean("saved_"+setting, SettingSyncHelper.Send(setting, str));
									edit.commit();
								} 
								catch (Exception e)
								{
									
								}
										
								
							}

							return null;
						}
						
						@Override
						public void result(ThreadStatus aStatus, D aResult)
						{
							
						}
					});
		}

		
		protected boolean isSynced()
		{
			

			return getSP().getBoolean("sync_"+setting, false);
			
			
		}
		
		protected boolean isSaved()
		{

			return getSP().getBoolean("saved_"+setting, true);
		}
		
		void init()
		{
			options = Base64.GZIP;
			
			is_sync = isSynced();
			
			if(!isSaved())
			{
				Save();
			}
			
			if(!is_sync && isSaved())
			new Handler(Looper.getMainLooper()).post(new Runnable()
			{
				
				@Override
				public void run()
				{
					ThreadTransanction.execute_http("sync banana get "+getSettingName(), new ThreadRunnable<D>()
					{
						
						@SuppressWarnings("unchecked")
						@Override
						public D run()
						{
							
							String str = SettingSyncHelper.Get(setting);

							//BAssert.log("SettingSync.Get "+setting+" is_null "+(str==null));
							
							if(str == null)
							{
								return null;
							}
							
							if(str.equals("null"))
							{
								Editor edit = getEditor();
								is_sync = true;
								edit.putBoolean("sync_"+setting, true);
								edit.commit();
								
								//BAssert.log("SettingSync sync_"+setting+" saved");
								
								return def;
							}
							
							D v = null;
							
							D objLocal = Get();
							
							if(str.length() == 0)
							{
								

							}
							else
							{
								
								try
								{
									
									v = (D) Base64.decodeToObject(str, options, loader);

									
									if(v!=null && objLocal!=null)
									{
										v = merge(objLocal, v);
									}
									
								}
								catch (Exception e)
								{
									e.printStackTrace();
								}
							}
						
							
							
							Editor edit = getEditor();
							is_sync = true;
							edit.putBoolean("sync_"+setting, true);
							edit.commit();
							
							//BAssert.log("SettingSync sync_"+setting+" saveed");
							
							return v;
						}
						
						@Override
						public void result(ThreadStatus aStatus, D aResult)
						{
							if(aResult!=null)
							{
								StorageSync.super.Save(aResult);
							}
						}
					});
					
					
					
				}
			});
		}
		
		
		
		public StorageSync(Integer setting, D def)
		{
			super(setting);
			
			init();
			
			this.def = def;
		}


		public StorageSync(Integer setting)
		{
			super(setting);
			
			init();
			
			
			
		}
		
	}
	
	public static class StorageDelayed<D extends Serializable> extends Storage<D> {

		
		
		public StorageDelayed(Integer setting) {
			super(setting);
		}

		@SuppressWarnings("rawtypes")
		public StorageDelayed(Integer setting, Class cl) {
			super(setting, cl);
		}

		public StorageDelayed(Integer setting, D def) {
			super(setting, def);
		}
		
		

		private void SaveNow()
		{
			super.Save(super.Get());
		}
		
		public Integer delay = 5000;
		WeakHandler handler;
		boolean is_saving_handler = false;
		@Override
		public void Save(D v)
		{
			
			value = v;
			
			if(handler == null)
			{
				handler = new WeakHandler(Looper.getMainLooper());
			}

			if(!is_saving_handler)
			{
				is_saving_handler = true;

				handler.postDelayed(new Runnable()
				{

					@Override
					public void run()
					{
						try {
							SaveNow();
						} 
						catch (Exception e) {
							e.printStackTrace();
						}

						is_saving_handler = false;
					}
					
				}, delay);
			}

		}
		
	}


	
}
