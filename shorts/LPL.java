package com.utils.shorts;

import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

public class LPL
{

	//layoutparam builder for AbsListView
	
	final public static Integer WRAP = -2;
	
	final public static Integer MATCH = -1;
	
	LinearLayout.LayoutParams lp_r;
	
	private LPL(LinearLayout.LayoutParams lp)
	{
		lp_r = lp;
	}
	
	public static LPL create(int w, int h)
	{
		return new LPL(new LinearLayout.LayoutParams(w, h));
	}
	
	public static LPL create(int h)
	{
		return new LPL(new LinearLayout.LayoutParams(-1, h));
	}
	
	public LPL margins(Integer... margins)
	{
		int len = margins.length;
		
		lp_r.leftMargin = margins[0];
		
		if(len >= 2)
		{
			lp_r.topMargin = margins[1];
		}
		
		if(len >= 3)
		{
			lp_r.rightMargin = margins[2];
		}
		
		if(len >= 4)
		{
			lp_r.bottomMargin = margins[3];
		}
		
		return this;
	}
	
	
	public LPL marginTop(int margin)
	{
		lp_r.topMargin = margin;
		
		return this;
	}

	public LPL gravity(int gravity)
	{
		lp_r.gravity = gravity;

		return this;
	}
	
	public LPL marginBottom(int margin)
	{
		lp_r.bottomMargin = margin;
		
		return this;
	}
	
	public LPL marginLeft(int margin)
	{
		lp_r.leftMargin = margin;
		
		return this;
	}
	
	public LPL marginRight(int margin)
	{
		lp_r.rightMargin = margin;
		
		return this;
	}
	
	public LPL weight(float weight)
	{
		
		lp_r.weight = weight;
		
		return this;
	}

	public LayoutParams get()
	{
		
		
		return lp_r;
	}
	
}
