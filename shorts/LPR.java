package com.utils.shorts;

import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class LPR
{

	//layoutparam builder for RelativeLayout
	
	final public static Integer WRAP = -2;
	
	final public static Integer MATCH = -1;
	
	RelativeLayout.LayoutParams lp_r;
	View view;
	ViewGroup parent;
	public boolean isRight = false;
	public boolean isBottom = false;
	
	private LPR(RelativeLayout.LayoutParams lp)
	{
		lp_r = lp;
	}
	
	private LPR(View view, RelativeLayout.LayoutParams lp)
	{
		lp_r = lp;
		this.view = view;
	}
	
	public static LPR create(int w, int h)
	{
		return new LPR(new RelativeLayout.LayoutParams(w, h));
	}
	
	
	
	public static LPR create(int h)
	{
		return new LPR(new RelativeLayout.LayoutParams(MATCH, h));
	}
	
	public static LPR createMW()
	{
		return new LPR(new RelativeLayout.LayoutParams(MATCH, WRAP));
	}
	
	public static LPR create()
	{
		return createMatch();
	}
	
	public static LPR createW(int w)
	{
		return create(w, LPR.MATCH);
	}

	public static LPR createMatch()
	{
		return new LPR(new RelativeLayout.LayoutParams(MATCH, MATCH));
	}
	
	public static LPR createWrap()
	{
		return new LPR(new RelativeLayout.LayoutParams(WRAP, WRAP));
	}

	public LPR wrapW()
	{
		lp_r.width = WRAP;
		
		return this;
	}
	
	public LPR forView(View view)
	{
		this.view = view;
		return this;
	}
	
	public LPR parent(ViewGroup parent)
	{
		this.parent = parent;
		return this;
	}
	
	public LPR margin(Integer margin)
	{
		return margins(margin,margin,margin,margin);
	}
	
	public LPR margins(Integer... margins)
	{
		int len = margins.length;

		if(len >= 1) {
			lp_r.leftMargin = margins[0];
		}

		if(len >= 2)
		{
			lp_r.topMargin = margins[1];
		}
		
		if(len >= 3)
		{
			lp_r.rightMargin = margins[2];
		}
		
		if(len >= 4)
		{
			lp_r.bottomMargin = margins[3];
		}
		
		return this;
	}
	
	public LPR below(Integer id)
	{
		lp_r.addRule(RelativeLayout.BELOW, id);
		
		return this;
	}
	
	public LPR centerH()
	{
		lp_r.addRule(RelativeLayout.CENTER_HORIZONTAL, 1);
		
		return this;
	}
	
	public LPR centerV()
	{
		lp_r.addRule(RelativeLayout.CENTER_VERTICAL, 1);
		
		return this;
	}
	
	public LPR calcCenterV(Integer forHeight)
	{
		if(lp_r.height > 0)
			lp_r.topMargin = lp_r.topMargin + (forHeight - lp_r.height) / 2;
		
		return this;
	}
	
	public LPR calcLineV(Integer width)
	{
		lp_r.height = LPR.MATCH;
		lp_r.width = width;
		
		return this;
	}
	
	public LPR calcLineH(Integer height)
	{
		lp_r.height = height;
		lp_r.width = LPR.MATCH;
		
		return this;
	}
	
	public LPR calcCenterH(Integer forWidth)
	{
		if(lp_r.width > 0)
			lp_r.leftMargin = lp_r.leftMargin + (forWidth - lp_r.width) / 2;
		
		return this;
	}
	
	public LPR center()
	{
		lp_r.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
		
		return this;
	}
	
	public LPR wrapH()
	{
		lp_r.height = WRAP;
		
		return this;
	}
	
	public LPR bottom()
	{
		isBottom = true;
		lp_r.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 1);
		
		return this;
	}
	
	public LPR right()
	{
		isRight = true;
		lp_r.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 1);
		
		return this;
	}
	

	public LPR marginTop(int margin)
	{
		lp_r.topMargin = margin;
		
		return this;
	}
	
	public LPR marginBottom(int margin)
	{
		lp_r.bottomMargin = margin;
		
		return this;
	}
	
	public LPR marginLeft(int margin)
	{
		lp_r.leftMargin = margin;
		
		return this;
	}
	
	public LPR marginRight(int margin)
	{
		lp_r.rightMargin = margin;
		
		return this;
	}

	public LPR rightOf(int id)
	{
		lp_r.addRule(RelativeLayout.RIGHT_OF, id);

		return this;
	}
	
	public LPR leftOf(int id)
	{
		lp_r.addRule(RelativeLayout.LEFT_OF, id);

		return this;
	}
	
	public RelativeLayout.LayoutParams get()
	{
		return lp_r;
	}
	
	public RelativeLayout.LayoutParams commit()
	{
		if(view!=null) {
			if(parent!=null) {
				parent.addView(view, lp_r);
			}
			else {
				view.setLayoutParams(lp_r);
			}
		}
		
		return lp_r;
	}
	
}
