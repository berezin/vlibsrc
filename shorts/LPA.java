package com.utils.shorts;

import android.widget.AbsListView;

public class LPA
{

	//layoutparam builder for AbsListView
	
	final public static Integer WRAP = -2;
	
	final public static Integer MATCH = -1;
	
	AbsListView.LayoutParams lp_r;
	
	private LPA(AbsListView.LayoutParams lp)
	{
		lp_r = lp;
	}
	
	
	public static LPA create(int w, int h)
	{
		return new LPA(new AbsListView.LayoutParams(w, h));
	}
	
	public static LPA create(int h)
	{
		return new LPA(new AbsListView.LayoutParams(-1, h));
	}

	public AbsListView.LayoutParams get()
	{
		return lp_r;
	}
	
}
