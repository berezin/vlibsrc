package com.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyActivity extends FragmentActivity
{

	
	public static interface IActivityHandler {

		void onDestroy();
		void onStart();
		void onStop();
		boolean isBlockBack();
		
	}
	
	public static interface IStateSaver {
		
		String getSaveStateId();
		Object saveState();
		void restoreState(Object o);
		Context getContext();
		
	}

	public static boolean AddStateSaverAndRestore(IStateSaver statesaver) {

		if(statesaver.getContext() instanceof MyActivity) {
			MyActivity activity = ((MyActivity)statesaver.getContext());
			
			activity.list_statesavers.add(statesaver);
			
			return activity.Restore(statesaver);
		}

		return false;
	}
	
	ArrayList<IActivityHandler> list_handlers = new ArrayList<MyActivity.IActivityHandler>();
	ArrayList<IStateSaver> list_statesavers = new ArrayList<IStateSaver>();


	public static void AddHandler(Context context, Integer id) {

		if (context instanceof MyActivity) {
			((MyActivity) context).AddHandler(id);
		}
	}

	public void AddHandler(Integer id) {
		try {
			list_handlers.add((IActivityHandler) findViewById(id));
		} catch (Exception e) {

		}
	}

	public static void AddHandler(Context context, View v) {
		if (context instanceof MyActivity) {
			((MyActivity) context).AddHandler(v);
		}
	}

	public void RemoveHandler(View v) {
		try {
			list_handlers.remove((IActivityHandler) v);
		} catch (Exception e) {
		}
	}

	public void AddHandler(View v) {
		try {
			list_handlers.add((IActivityHandler) v);
		} catch (Exception e) {
		}
	}

	public static Integer W() {

		try {
			Rect rectgle = new Rect();
			Window window = CURRENT_ACTIVITY.getWindow();
			window.getDecorView()
				  .getWindowVisibleDisplayFrame(rectgle);

			return rectgle.right - rectgle.left;
		} catch (Exception e) {

		}

		return App.get().getResources().getDisplayMetrics().widthPixels;
	}

	public static Integer H() {


		try {
			Rect rectgle = new Rect();

			Window window = CURRENT_ACTIVITY.getWindow();
			int[] mass = new int[4];
			window.getDecorView()
				  .getLocationOnScreen(mass);

			window.getDecorView()
				  .getWindowVisibleDisplayFrame(rectgle);

			int w = window.getAttributes().width;

			return rectgle.bottom - rectgle.top;
		} catch (Exception e) {

			//e.printStackTrace();
		}


		return App.get().getResources()
								.getDisplayMetrics().heightPixels;
	}
	
	
	HashMap<String, Object> map_savestate = null;
	
	static HashMap<Integer, ArrayList<MyActivity>> map_stacks = new HashMap<Integer, ArrayList<MyActivity>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		//E.Bind(this);

		CURRENT_ACTIVITY = this;
		super.onCreate(savedInstanceState);
		try {
			map_savestate = (HashMap<String, Object>) getLastCustomNonConfigurationInstance();
		} catch (Exception e) {

		}


		try {
			if (isRestrictedStack()) {

				if (!map_stacks.containsKey(getStackID())) {
					map_stacks.put(getStackID(), new ArrayList<MyActivity>());
				}

				ArrayList<MyActivity> list = map_stacks.get(getStackID());

				list.add(this);

				while (list.size() > getStackMax()) {
					list.remove(0).finish();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}


	}


	//Stack

	//есть ли ограничение
	protected boolean isRestrictedStack() {
		return false;
	}

	//id стека
	protected int getStackID() {
		return 0;
	}

	// максимальное кол-во экранов в стеке
	protected int getStackMax() {
		return 3;
	}


	private boolean Restore(IStateSaver s) {

		if (map_savestate != null && map_savestate.containsKey(s.getSaveStateId())) {
			s.restoreState(map_savestate.get(s.getSaveStateId()));

			map_savestate.remove(s.getSaveStateId());

			return true;
		}

		return false;
	}

	@Override
	protected void onResume() {

		super.onResume();


		if (map_savestate != null) {

			for (IStateSaver s : list_statesavers) {

				if (map_savestate.containsKey(s.getSaveStateId())) {
					s.restoreState(map_savestate.get(s.getSaveStateId()));

					map_savestate.remove(s.getSaveStateId());
				}

			}
		}
	}

	@Override
	public Object onRetainCustomNonConfigurationInstance() {

		map_savestate = new HashMap<String, Object>();

		for (IStateSaver s : list_statesavers) {
			map_savestate.put(s.getSaveStateId(), s.saveState());
		}

		return map_savestate;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
		super.onSaveInstanceState(outState);
	}


	@Override
	protected void onStart() {

		for (IActivityHandler handler : list_handlers) {
			handler.onStart();
		}

		super.onStart();
	}

	@Override
	protected void onStop() {

		for (IActivityHandler handler : list_handlers) {
			handler.onStop();
		}

		super.onStop();
	}

	@Override
	protected void onDestroy() {

		//E.Unbind(this);


		if (CURRENT_ACTIVITY != null && this.getComponentName().equals(CURRENT_ACTIVITY.getComponentName())) {
			CURRENT_ACTIVITY = null;
		}

		for (IActivityHandler handler : list_handlers) {
			handler.onDestroy();
		}

		list_handlers = new ArrayList<MyActivity.IActivityHandler>();
		list_statesavers = new ArrayList<MyActivity.IStateSaver>();

		super.onDestroy();


		try {
			if (isRestrictedStack()) {

				if (!map_stacks.containsKey(getStackID())) {
					map_stacks.put(getStackID(), new ArrayList<MyActivity>());
				}

				ArrayList<MyActivity> list = map_stacks.get(getStackID());

				list.remove(this);

			}
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	static public Activity CURRENT_ACTIVITY;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {


			for (IActivityHandler ia : list_handlers) {

				if (ia.isBlockBack()) {
					return true;
				}
				
			}
			
			return super.onKeyDown(keyCode, event);

		} else
		{
			return super.onKeyDown(keyCode, event);
		}

	}

	public static boolean hasOpenActivity()
	{
		return !isApplicationBroughtToBackground();
	}

	public static boolean isApplicationBroughtToBackground() {
		
		
	    ActivityManager am = (ActivityManager) App.get().getSystemService(Context.ACTIVITY_SERVICE);
	    List<RunningTaskInfo> tasks = am.getRunningTasks(1);
	    if (!tasks.isEmpty()) {
	        ComponentName topActivity = tasks.get(0).topActivity;
	        if (!topActivity.getPackageName().equals(App.get().getPackageName())) {
	            return true;
	        }
	    }

	    return false;
	}
	
	
}