package com.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Xfermode;
import android.text.Layout.Alignment;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;

public class CanvasUtils
{
	
	private static final String ELLIPSIS = "...";
    final public static Interpolator interpolator_accelerate = new AccelerateInterpolator();
    final public static Interpolator interpolator_accelerate_decelerate = new AccelerateDecelerateInterpolator();

    public static StaticLayout createWorkingLayout(CharSequence workingText, TextPaint tp, int w) {
		return new StaticLayout(workingText, tp, w, Alignment.ALIGN_NORMAL, 1, 1, false);
	}
	
	public static StaticLayout trimText(Spanned fullString, int maxLines, TextPaint tp, Integer w)
	{
		
		
        CharSequence workingText = fullString;

        if (maxLines != -1) 
        {
        	
        	StaticLayout layout = createWorkingLayout(workingText,tp,w);
            
        	if (layout.getLineCount() > maxLines) 
            {
            	
            	workingText = fullString.subSequence(0, layout.getLineEnd(maxLines - 1));
            	
            	try
				{
					while(workingText.charAt(workingText.length()-1) == ' ')
					{
						workingText = workingText.subSequence(0, workingText.length()-1);
					}
				}
            	catch (Exception e)
				{
					
					e.printStackTrace();
				}
            	
            	layout = createWorkingLayout(new SpannedString(new SpannableStringBuilder(workingText).append(ELLIPSIS)),tp,w);
                
                while (layout.getLineCount() > maxLines) {
                   
                    
                    String s = workingText.toString();
                    
                    int lastSpace = s.lastIndexOf(' ');
                    
                    if (lastSpace == -1) {
                        break;
                    }
                    
                    workingText = workingText.subSequence(0, lastSpace);
                    
                  
                }
                
                
            }
        	
        	return layout;
        }
        
        return createWorkingLayout(workingText,tp, w);
		
	}
	
	public static StaticLayout trimChars(CharSequence fullString, int maxLines, TextPaint tp, Integer w)
	{
		
		
        CharSequence workingText = fullString;

        if (maxLines != -1) 
        {
        	
        	StaticLayout layout = createWorkingLayout(workingText,tp,w);
            
        	if (layout.getLineCount() > maxLines) 
            {
            	
            	workingText = fullString.subSequence(0, layout.getLineEnd(maxLines - 1));
            	
            	try
				{
					while(workingText.charAt(workingText.length()-1) == ' ')
					{
						workingText = workingText.subSequence(0, workingText.length()-1);
					}
				}
            	catch (Exception e)
				{
					
					e.printStackTrace();
				}
            	
            	layout = createWorkingLayout(new SpannedString(new SpannableStringBuilder(workingText).append(ELLIPSIS)),tp,w);
                
                while (layout.getLineCount() > maxLines) {
                   
                    
                    String s = workingText.toString();
                    
                    int lastSpace = s.lastIndexOf(' ');
                    
                    if (lastSpace == -1) {
                        break;
                    }
                    
                    workingText = workingText.subSequence(0, lastSpace);
                    
                  
                }
                
                
            }
        	
        	return layout;
        }
        
        return createWorkingLayout(workingText,tp, w);
		
	}
	/*
	public static StaticLayout trimLine(String workingText, TextPaint tp, Integer w)
	{
		if(workingText == null)
		 {
			 workingText = "";
		 }
		 
		
		 workingText = ""+TextUtils.ellipsize(workingText, tp, (float)w, TruncateAt.END);
		 
		 return createWorkingLayout(workingText,tp, w);

	}
	
	public static StaticLayout trimText(String fullString, int maxLines, TextPaint tp, Integer w)
	{
		
        String workingText = fullString;
        
        if (maxLines != -1) {
        	StaticLayout layout = createWorkingLayout(workingText,tp, w);
            if (layout.getLineCount() > maxLines) {
            	
            	
            	
                workingText = fullString.substring(0, layout.getLineEnd(maxLines - 1)).trim();
                
                layout = createWorkingLayout(workingText + ELLIPSIS,tp, w);
                
                while (layout.getLineCount() > maxLines) {
                	
                	
                	
                    int lastSpace = workingText.lastIndexOf(' ');
                    
                    if (lastSpace == -1) {
                        break;
                    }
                    
                    workingText = workingText.substring(0, lastSpace);
                    
                    layout = createWorkingLayout(workingText + ELLIPSIS,tp, w);
                }
                
                return layout;
                //workingText = workingText + ELLIPSIS;
            }
            else
            {
            	return layout;
            }
            
            
        }
      
        return createWorkingLayout(workingText,tp, w);
        
	}
	*/
	
	public static Rect DrawImage(Canvas canvas, Bitmap bitmap, Rect rectDestiny, Boolean isCrop)
	{
		return DrawImage(canvas, bitmap, rectDestiny, isCrop, true, 255, false);
	}
	
	static Paint p = new Paint();
	static Xfermode mode;
	static Xfermode mode_xfer;
	static ColorMatrix cm;
	static
	{
		p.setColor(0x00ffffff);
		mode = p.getXfermode();
		mode_xfer = new PorterDuffXfermode(Mode.SRC);

	}

	private static Rect getRectDrawing(Rect rectSrc, Rect rectDestiny, Boolean crop)
	{
		
		
		
		float kw = (float) (rectDestiny.width())/ rectSrc.width();
		
		float kh =  (float)(rectDestiny.height())/ rectSrc.height();
		
		boolean isHorizontalTrim;
		float k;
		
		if(!crop) {
			isHorizontalTrim = kw < kh;
			k = Math.min(kw, kh);
		}
		else {
			 isHorizontalTrim = kw > kh;
			 k = Math.max(kw, kh);
		}
		
		float w = (rectSrc.width()) * k, h = (rectSrc.height()) * k;
		
		float r_x = isHorizontalTrim?0:( (rectDestiny.width()) - w  )/2;
		float r_y = !isHorizontalTrim?0:( (rectDestiny.height()) - h  )/2;
		
		Rect r = new Rect( (int)(rectDestiny.left + r_x),  (int)(rectDestiny.top+r_y),  (int)(rectDestiny.right - r_x),  (int)(rectDestiny.bottom-r_y));
		
		return r;
	}

	public static void DrawImage(Canvas c, Bitmap b, Rect rect_clip, Rect rect_for_drawing, Rect rectSrc, Boolean is_filter, int alpha, boolean is_xfer_src) {

		if(b == null || b.isRecycled()) {
			
			return;
		}
		
		c.save();
		
		if(c.getClipBounds().contains(rect_clip)) {
            c.clipRect(rect_clip);
        }

		if(is_xfer_src && alpha==255) {
			p.setXfermode(mode_xfer);
		}
		else {
			p.setXfermode(mode);
		}

		p.setDither(true);
		p.setAntiAlias(is_filter);
		p.setFilterBitmap(is_filter);
		p.setAlpha(alpha);

		c.drawBitmap(b, null, rect_for_drawing, p);
		
	
		
		//c.clipRect(new Rect(0,0,c.getWidth(),c.getHeight()),Op.REPLACE);
		
		c.restore();
	
	}



	public static Rect DrawImage(Canvas c, Bitmap b, Rect rect_clip, Boolean crop, Boolean is_filter, int alpha, boolean is_xfer_src) {

        if(b == null) {
			return null;
		}

		Rect rectSrc = new Rect(0, 0, b.getWidth(), b.getHeight());
		
		Rect r = getRectDrawing(rectSrc, rect_clip, crop);

		DrawImage(c, b, rect_clip, r, rectSrc, is_filter, alpha, is_xfer_src);
		
		return r;
	}

	public static void DrawImageXY(Canvas c, Bitmap b, Rect rect, boolean is_filter) {
		 DrawImageXY(c, b, rect, is_filter, 255);
	}

	public static void DrawImageXY(Canvas c, Bitmap b, Rect rect, boolean is_filter, int alpha) {
		c.save();
		
		Rect rectSrc = new Rect(0, 0, b.getWidth(), b.getHeight());
		//Rect rectCanv = c.getClipBounds();
		
		
		
		p.setAlpha(alpha);

		p.setFilterBitmap(is_filter);
		
		
		c.drawBitmap(b, rectSrc, rect, p);
		
		c.restore();
	}
}
