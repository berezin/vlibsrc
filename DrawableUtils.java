package com.utils;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.view.View;

/**
 * Created by berezin-ds on 06.07.2015.
 */
public class DrawableUtils {

	@SuppressWarnings("deprecation")
	public static void Set(View view, Drawable d) {
		view.setBackgroundDrawable(d);
	}

    public static Drawable StrokeAndFill(Integer stroke_color, int width, Integer fill_color)
    {

        return new LayerDrawable(new Drawable[]{Fill(fill_color),Stroke(stroke_color, width)});

    }

    public static Drawable Round(Integer color, float round)
    {

        ShapeDrawable shape = new ShapeDrawable(
                new RoundRectShape(color, round));
        
        return shape;
    }
    
    public static Drawable Oval(Integer color)
    {

        ShapeDrawable shape = new ShapeDrawable(
                new CircleRectShape(color));
        
        return shape;
    }
    
    public static Drawable OvalStroke(Integer color, float width)
    {

        ShapeDrawable shape = new ShapeDrawable(
                new CircleRectShape(color, width));
        
        return shape;
    }

    public static Drawable RoundStroke(Integer color, float width, float round)
    {

        ShapeDrawable shape = new ShapeDrawable(
                new RoundRectShape(color, width, round));

        return shape;
    }

    public static Drawable Stroke(Integer color, int width)
    {


        ShapeDrawable shape = new ShapeDrawable( new RectShape());
        shape.getPaint().setColor(color);
        shape.getPaint().setStyle(Style.STROKE);
        shape.getPaint().setStrokeWidth(width);

        return shape;
    }

    public static Drawable Fill(Integer color)
    {


        ShapeDrawable shape = new ShapeDrawable( new RectShape());
        shape.getPaint().setColor(color);
        shape.getPaint().setStyle(Style.FILL);

        return shape;
    }

    public static Drawable Highlight(Integer color_normal, Integer color_pressed)
    {

        StateListDrawable states = new StateListDrawable();

        states.addState(new int[] {android.R.attr.state_pressed},
                        Fill(color_pressed));

        states.addState(new int[] {},
                        Fill(color_normal));

        return states;


    }



    public static class RoundRectShape extends RectShape {

        float round;
        Paint mPaint;
        boolean isStroke;

        public RoundRectShape(final Integer color, final float width, float round) {
            this.round = round;
            isStroke = true;


            mPaint = new Paint(){{
                setColor(color);
                setAntiAlias(true);
                setStyle(Style.STROKE);
                setStrokeWidth(width);
            }};

        }

        public RoundRectShape(final Integer color, float round) {
            this.round = round;
            isStroke = false;

            mPaint = new Paint(){{
                setColor(color);
                setAntiAlias(true);
            }};

        }

        @Override
        public void draw(final Canvas canvas, final Paint paint) {

            paint.set(mPaint);
            float pad = isStroke?round / 2:0;
            float w = getWidth();
            float h = getHeight();
            canvas.drawRoundRect(new RectF(pad, pad, w - pad, h - pad),round,round, paint);

        }
    }
    
    public static class CircleRectShape extends RectShape {

        Paint mPaint;
        boolean isStroke;
        float strokeWidth = 0;
        
        public CircleRectShape(final Integer color, final float width) {
     
            isStroke = true;
            strokeWidth = width;

            mPaint = new Paint(){{
                setColor(color);
                setAntiAlias(true);
                setStyle(Style.STROKE);
                setStrokeWidth(width);
            }};

        }

        public CircleRectShape(final Integer color) {
    
            isStroke = false;

            mPaint = new Paint(){{
                setColor(color);
                setAntiAlias(true);
            }};

        }

        @Override
        public void draw(final Canvas canvas, final Paint paint) {

            paint.set(mPaint);
            float pad = isStroke?strokeWidth / 2:0;
            float w = getWidth();
            float h = getHeight();
            canvas.drawOval(new RectF(pad, pad, w - pad, h - pad), paint);

        }
    }

}
