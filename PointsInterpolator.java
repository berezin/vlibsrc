package com.utils;

import android.graphics.Color;
import android.graphics.Path;
import android.graphics.PointF;
import java.util.ArrayList;

public class PointsInterpolator
{

	public static class DataPointsState {
		
		ArrayList<Float> floats = new ArrayList<Float>();
		
		private DataPointsState() {
		}

		public DataPointsState addLine(DataPointsLine out, float x1, float y1, float x2, float y2) {

			out.set(floats.size());

			return addLine(x1, y1, x2, y2);
		}

		public DataPointsState addLine(float x1, float y1, float x2, float y2) {
			floats.add(x1);
			floats.add(y1);
			floats.add(x2);
			floats.add(y2);

			return this;
		}


		
		
		public DataPointsState addColor(DataPointsColor out, Integer color) {
			
			out.set(floats.size());
			
			return addColor(color);
		}
		
		public DataPointsState addColor(Integer color) {
			
			float a = Color.alpha(color);
			float r = Color.red(color);
			float g = Color.green(color);
			float b = Color.blue(color);
			
			floats.add(a);
			floats.add(r);
			floats.add(g);
			floats.add(b);
			
			return this;
		}

		//addPath
		public DataPointsState addPath(DataPointsPath out, boolean is_closed, PointF... points) {

			out.set(floats.size(), points.length, is_closed);

			return addPath(points);
		}

		public DataPointsState addPath(PointF... points) {

			for(PointF p:points) {
				floats.add(p.x);
				floats.add(p.y);
			}

			return this;
		}

		//addPathLine
		public DataPointsState addPathLine(DataPointsPath out, PointF point1, PointF point2, int pointsCount) {
			out.set(floats.size(), pointsCount, false);
			return addPathLine(point1, point2, pointsCount);
		}

		public DataPointsState addPathLine(PointF point1, PointF point2, int pointsCount) {
			float dy = (point2.y - point1.y) / (pointsCount - 1);
			float dx = (point2.x - point1.x) / (pointsCount - 1);
			for(int i = 0; i < pointsCount; i++) {
				floats.add(point1.x + i * dx);
				floats.add(point1.y + i * dy);
			}
			return this;
		}

		//addPathArc
		public DataPointsState addPathArc(DataPointsPath out,PointF pointStart, PointF pointCenter, float degrees, int pointsCount,
										  boolean clockDirection) {
			out.set(floats.size(), pointsCount, false);
			return addPathArc(pointStart, pointCenter, degrees, pointsCount, clockDirection);
		}

		public DataPointsState addPathArc(PointF pointStart, PointF pointCenter, float degrees, int pointsCount,
										  boolean clockDirection) {


			float dAngle = degrees / (pointsCount - 1) * (clockDirection?1f:-1f);
			float r = MathUtils.getR(pointStart.x, pointStart.y, pointCenter.x, pointCenter.y);

			float angleStart = MathUtils.getAngle(pointStart.x, pointStart.y, pointCenter.x, pointCenter.y);

			for(int i = 0; i < pointsCount; i++) {

				float angle = angleStart + i * dAngle;

				floats.add(pointCenter.x + MathUtils.getX(angle, r));
				floats.add(pointCenter.y + MathUtils.getY(angle, r));

			}

			return this;
		}

		//addFloat
		public DataPointsState addFloat(DataPointsFloat out, Float value) {
			out.set(floats.size());

			return addFloat(value);
		}

		public DataPointsState addFloat(Float value) {
			floats.add(value);
			return this;
		}
		
		
		public static DataPointsState create()
		{
			return new DataPointsState();
		}
		
		public Float[] get()
		{
			Float[] contents = new Float[floats.size()];
			
			return floats.toArray(contents);
		}
		
	}
	
	
	public static class DataPointsPath
	{
		
		
		int offset;
		int count;
		boolean is_closed;
		
		public void set(int offset, int count, boolean is_closed)
		{
			this.offset = offset;
			this.count = count;
			this.is_closed = is_closed;
		}
		
		Path path;
		
		public Path get(PointsInterpolator vpoints)
		{
			
			if(vpoints.isChanged() || path == null)
			{
				if(path == null)
				{
					path = new Path();
				}
				
				path.reset();
				
				
				path.moveTo(vpoints.current_state[offset], vpoints.current_state[offset+1]);
				int d = 0;
				for(int i = 1; i < count; i++)
				{
					
					d = offset + i * 2;
					
					path.lineTo(
							vpoints.current_state[d],
							vpoints.current_state[d + 1]);
					
					
				}
				
				path.lineTo(
						vpoints.current_state[d],
						vpoints.current_state[d + 1]);
				
				if(is_closed)
				{
					path.close();
				}
	
			}
			
			return path;
		}
		
		
	}

	public static class DataPointsLine
	{


		int offset;

		public void set(int offset)
		{
			this.offset = offset;
		}

		float[] pts;

		public float[] get(PointsInterpolator vpoints)
		{

			if(vpoints.isChanged() || pts == null)
			{

				if(pts == null) {
					pts = new float[4];
				}

				for(int i = 0; i < 4; i++) {
					pts[i] = vpoints.current_state[offset + i];
				}
			}

			return pts;
		}


	}
	
	public static class DataPointsColor
	{
		
		
		int offset;
		
		public void set(int offset)
		{
			this.offset = offset;
		}
		
		Integer color;
		
		public Integer get(PointsInterpolator vpoints)
		{
			
			if(vpoints.isChanged() || color == null)
			{
				
				int a = vpoints.current_state[offset].intValue();
				int r = vpoints.current_state[offset+1].intValue();
				int g = vpoints.current_state[offset+2].intValue();
				int b = vpoints.current_state[offset+3].intValue();
				
				color = Color.argb(a,r,g,b);
				
				
	
			}
			
			return color;
		}
		
		
	}

	public static class DataPointsFloat
	{


		int offset;

		public void set(int offset)
		{
			this.offset = offset;
		}

		Float value;

		public Float get(PointsInterpolator vpoints)
		{

			if(vpoints.isChanged() || value == null)
			{
				value = vpoints.current_state[offset];
			}

			return value;
		}


	}
	
	public static class DataTimeline
	{
		
		public Integer[] states;
		
		public float[] weights;
		
		public boolean cycle;
		
		public long dur;
		
		
		
		
		Timeline timeline;
		
		boolean is_first = true;
		Integer to_state = 0;
		Float[] first_state;
		
		
		public DataTimeline(Integer[] states, float[] weights, boolean cycle, long dur)
		{
			this.states = states;
			this.weights = weights;
			this.cycle = cycle;
			this.dur = dur;
		}
		
		public boolean isEnd()
		{
			return timeline == null;
		}
		
		void createTimeline()
		{
			float f_max = 0f;
			
			for(float f : weights)
			{
				f_max+=f;
			}
			
			timeline = new Timeline(dur, f_max);
			
			
			
		}
		
		public void calc(PointsInterpolator points)
		{
			
			if(timeline == null)
			{
				first_state = points.current_state.clone();
				
				createTimeline();
				
			}
			
			
			
			
			
			

			if(timeline.isEnd())
			{
				is_first = false;
				
				if(cycle)
				{

					weights[0] = 0f;
					
					createTimeline();
					
					
				}
				else
				{
					timeline = null;
				}

			}
			else
			{
				timeline.step();
				
				float f = timeline.getValue();
				float f1 = 0f;
				
				Float[] from;
				Float[] to;
				
				
				int i = 0;
				
				
				for(i = 0; i < weights.length; i++)
				{
					
					if(f1 <= f &&
							f < f1 + weights[i])
					{
						break;
					}
					
					if(i!=weights.length - 1)
					{
						f1 += weights[i];
					}
					else
					{
						break;
					}
					
				}
				
				if(is_first && i == 0)
				{
					from = first_state;
					to = points.getState(states[0]);
				}
				else
				{
					from = points.getState(states[i-1]);
					to = points.getState(states[i]);
				}
				
				float k = formalize(
						f,
						f1,
						f1 + weights[i]);
				
				for(int j = 0; j < from.length; j++)
				{
					points.current_state[j] = from[j] + (to[j] - from[j])*k;						
				}
				
			}
			
		}
		
		
		
	}
	
	
	ArrayList<Float[]> list_states = new ArrayList<>();
	Float[] current_state;
	boolean is_changed = true;
	
	DataTimeline data_timeline;
	
	public boolean isChanged()
	{
		return is_changed;
	}
	
	public boolean isActive()
	{
		return data_timeline!=null;
	}
	
	public void addState(Float[] floats)
	{
		list_states.add(floats);
		
		if(current_state == null)
		{
			current_state = new Float[floats.length];
			
			setState(0);
		}
		
	}
	
	public Float[] getState(Integer state)
	{
		return list_states.get(state);
	}
	
	public void setState(Integer state)
	{
		data_timeline = null;
		
		Float[] floats = list_states.get(state);
		
		for(int i = 0; i < floats.length; i++)
		{
			current_state[i] = floats[i];
		}
	}
	
	public void calc()
	{
		if(data_timeline!=null)
		{
			is_changed = true;
			
			data_timeline.calc(this);
			
			if(data_timeline.isEnd())
			{
				data_timeline = null;
			}
		}
		else
		{
			is_changed = false;
		}

	}
	
	public void toState(Integer state, long dur)
	{
		
		data_timeline = new DataTimeline(new Integer[]{ state }, new float[]{1f}, false, dur);
		
	}
	
	public void toStates(Integer[] states, float[] weights, boolean cycle, long dur)
	{
		
		data_timeline = new DataTimeline(states, weights, cycle, dur);
		
	}
	
	
	
	public static float formalize(float k, float min, float max)
	{
		if(k < min)
		{
			k = 0f;
		}
		else if(k > max)
		{
			k = 1f;
		}
		else
		{
			k = (k - min)/(max - min);
		}
		
		return k;
	}

	public void stop()
	{
		
		data_timeline = null;
		
	}
	
}
