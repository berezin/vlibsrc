package com.utils;

import java.util.HashMap;
import java.util.Map;


import android.graphics.Typeface;

public class TypeUtils
{
	static Map<String, Typeface> types = new HashMap<String, Typeface>();
	
	
	public static Typeface Get(String name)
	{
		
		if(Utils.getOsVersion()<11)
		{
			return Typeface.DEFAULT;
		}
		
		
		Typeface tf = types.get(name);
		
		if(tf == null)
		{
			tf = Typeface.createFromAsset(App.get().getAssets(),"fonts/"+name);
			
			
			if(tf != null)
			{
				types.put(name, tf);
			}
		}
		
		
		
		return tf;
	}
	
	
}
