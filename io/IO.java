package com.utils.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;
import org.apache.http.util.ByteArrayBuffer;

import com.utils.App;
import com.utils.Utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory.Options;

@SuppressWarnings("deprecation")
public class IO
{

	
	public static final int IO_BUFFER_SIZE = 8 * 1024;
	
	public static class HttpClientFactory {

	    private static DefaultHttpClient client;

	    public synchronized static DefaultHttpClient getThreadSafeClient() {
	  
	        if (client != null)
	        {
	        	return client;
	        }
	         
	        client = new DefaultHttpClient();
	        
	        ClientConnectionManager mgr = client.getConnectionManager();
	        
	        HttpParams params = client.getParams();
	        
	        client = new DefaultHttpClient(
	        new ThreadSafeClientConnManager(params,
	            mgr.getSchemeRegistry()), params);
	  
	        return client;
	    } 
	}
	
	public static String FileNameFromUrl(String store, String url)
	{
		return Utils.DB_EXTERNAL_PATH() + store + "/"
				+ file_namer.getFileName(url) + "0";
	}
	
	public static String FileNameFromStore(String store)
	{
		return Utils.DB_EXTERNAL_PATH() + store + "/";
	}

	public static String StringFromAsset(String path)
	{
		StringBuilder buf = new StringBuilder();
		try
		{
			InputStream json = App.get().getAssets().open(path);
			BufferedReader in = new BufferedReader(new InputStreamReader(json));
			String str;

			while ((str = in.readLine()) != null)
			{
				buf.append(str);
			}

			in.close();
		} catch (Exception e)
		{

			e.printStackTrace();
		}

		return buf.toString();
	}
	
	public static void StringToFile(String store, String filename, String fileContents)
	{
		StringToFile(new File(IO.FileNameFromStore(store)+filename),fileContents);
	}
	
	public static void RemoveFile(String store, String filename)
	{
		try
		{
			new File(IO.FileNameFromStore(store)+filename).delete();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void StringToFile(File f, String fileContents)
	{

		FileWriter out = null;
		
		try
		{

			try
			{
				f.getParentFile().mkdirs();
			}
			catch (Exception e)
			{

			}

			out = new FileWriter(f);
			out.write(fileContents);
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		finally {
			
			if(out!=null) {
				try
				{
					out.close();
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	public static void BitmapToJPGFile(File f, Bitmap bitmap)
	{

		try
		{

			try
			{
				f.getParentFile().mkdirs();
			}
			catch (Exception e)
			{

			}

			OutputStream out = null;
			try
			{

				out = new BufferedOutputStream(new FileOutputStream(f),
						IO_BUFFER_SIZE);
				
				bitmap.compress(CompressFormat.JPEG, 75, out);
				
				
			} catch (Exception e)
			{
				e.printStackTrace();

			} finally
			{
				if(out != null)
				{
					IOUtils.closeQuietly(out);
					out = null;
				}
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void BitmapToPNGFile(File f, Bitmap bitmap)
	{
		
		try
		{

			try
			{
				f.getParentFile().mkdirs();
			} catch (Exception e)
			{

			}

			OutputStream out = null;
			try
			{

				out = new BufferedOutputStream(new FileOutputStream(f),
						IO_BUFFER_SIZE);
				
				bitmap.compress(CompressFormat.PNG, 50, out);
				
				
			} catch (Exception e)
			{
				e.printStackTrace();

			} finally
			{
				if(out != null)
				{
					out.close();
				}
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static byte[] BytesJPEGFromBitmap(Bitmap b)
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		b.compress(CompressFormat.JPEG, 75, out);
		
		return out.toByteArray();
	}
	
	public static byte[] BytesPNGFromBitmap(Bitmap b)
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		b.compress(CompressFormat.PNG, 40, out);
		
		return out.toByteArray();
	}
	
	
	public static void BytesToFile(File f, byte[] b)
	{
		try
		{

			try
			{
				f.getParentFile().mkdirs();
			} catch (Exception e)
			{

			}

			OutputStream out = null;
			InputStream in = null;
			
			try
			{

				out = new BufferedOutputStream(new FileOutputStream(f),
						IO_BUFFER_SIZE);
				
				in = new ByteArrayInputStream(b);
				
				IOUtils.copy(in, out);

			} catch (Exception e)
			{
				e.printStackTrace();
				
				if(out != null)
				{
					out.close();
					out = null;
				}
				
				try
				{
					if(f.exists())
					{
						f.delete();
					}
				} catch (Exception e1)
				{
					
				}

			} finally
			{
				if(out != null)
				{
					out.close();
				}
				
				if(in != null)
				{
					in.close();
				}
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static String StringFromObject(Object o)
	{

		try
		{
			return o == null ? "" : Base64.encodeObject((Serializable) o);
		} catch (Exception e)
		{
			return null;
		}

	}

	public static Object ObjectFromString(String str)
	{
		try
		{
			return str.equals("") ? null : Base64.decodeToObject(str);

		} catch (Exception e)
		{
			return null;
		}

	}
	
	public static File FileFromStoreAndName(String store, String file_name)
	{
		return new File(IO.FileNameFromStore(store)+file_name);
	}
	
	public static String StringFromFile(String store, String file_name)
	{
		return StringFromFile(new File(IO.FileNameFromStore(store)+file_name));
	}
	
	public static String StringFromFile(File f)
	{

		String str = null;
		
		try
		{
			if(f.exists())
			{

				str = FileUtils.readFileToString(f);// IoUtils.readWhole(fis);

			}
		} catch (Exception e)
		{

			e.printStackTrace();
		}

		return str;
	}
	
	public static byte[] BytesFromFile(File f)
	{
		byte[] b = null;
		
		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream(f);

			b = Read(fis);

			IOUtils.closeQuietly(fis);
			fis = null;
			
		} catch (Exception e)
		{

			e.printStackTrace();
		} finally
		{
			try
			{
				if(fis != null)
				{
					IOUtils.closeQuietly(fis);
				}
			} catch (Exception e)
			{

				e.printStackTrace();
			}
		}

		return b;
	}

	@SuppressWarnings("unused")
	public static File FileFromUrl(File file, String url)
	{
		BufferedInputStream stream = null;
		BufferedOutputStream out = null;

		try
		{
			
			HttpClient client = HttpClientFactory.getThreadSafeClient();
			HttpGet get = new HttpGet(url);
			get.setHeader("User-Agent", Utils.getUserAgent());
			
			File folder = file.getParentFile();
			folder.mkdirs();

			file.createNewFile();

			HttpResponse response = client.execute(get);

			InputStream is = response.getEntity().getContent();

			int buf_size = 8192;
			long file_size = response.getEntity().getContentLength();

			stream = new BufferedInputStream(is);

			out = new BufferedOutputStream(new FileOutputStream(file));

			long i = 0;
			byte buf[] = new byte[buf_size];
			int numread;

			while ((numread = stream.read(buf, 0, buf_size)) >= 0)
			{

				out.write(buf, 0, numread);

				i += numread;
			}

			stream.close();

			out.close();

			if(file_size != file.length())
			{
				return null;
			}

			return file;
		} catch (Exception ex)
		{

			try
			{
				if(stream != null)
				{
					// stream.reset();
					stream.close();
				}

			} catch (Exception e)
			{

			}

			try
			{
				if(out != null)
				{
					// stream.reset();
					out.close();
				}

			} catch (Exception e)
			{

			}

			try
			{
				file.delete();
			} catch (Exception e)
			{

				e.printStackTrace();
			}

			ex.printStackTrace();
		}

		return null;
	}
	
	@SuppressWarnings("unused")
	public static File FileFromUrl(String store, String url)
	{
		BufferedInputStream stream = null;
		BufferedOutputStream out = null;
		File file = new File(FileNameFromUrl(store, url));

		try
		{
			
			HttpClient client = HttpClientFactory.getThreadSafeClient();//new DefaultHttpClient();
			HttpGet get = new HttpGet(url);
			get.setHeader("User-Agent", Utils.getUserAgent());
			
			
			
			File folder = new File(Utils.DB_EXTERNAL_PATH() + store);
			folder.mkdirs();

			file.createNewFile();

			HttpResponse response = client.execute(get);

			InputStream is = response.getEntity().getContent();

			int buf_size = 8192;
			long file_size = response.getEntity().getContentLength();

			stream = new BufferedInputStream(is);

			out = new BufferedOutputStream(new FileOutputStream(file));

			long i = 0;
			byte buf[] = new byte[buf_size];
			int numread;

			while ((numread = stream.read(buf, 0, buf_size)) >= 0)
			{

				out.write(buf, 0, numread);

				i += numread;
			}

			stream.close();

			out.close();

			if(file_size != file.length())
			{
				return null;
			}

			return file;
		} catch (Exception ex)
		{

			try
			{
				if(stream != null)
				{
					// stream.reset();
					stream.close();
				}

			} catch (Exception e)
			{

			}

			try
			{
				if(out != null)
				{
					// stream.reset();
					out.close();
				}

			} catch (Exception e)
			{

			}

			try
			{
				file.delete();
			} catch (Exception e)
			{

				e.printStackTrace();
			}

			ex.printStackTrace();
		}

		return null;
	}

	public static Bitmap BitmapFromFile(File f)
	{

		try
		{
			return BitmapFactory.decodeFile(f.getPath(), Utils.getOptions());
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}
	
	public static byte[] BytesFromUrl(String url)
	{

		try
		{
			
			HttpClient client = HttpClientFactory.getThreadSafeClient();//client();//new DefaultHttpClient();
			
			HttpGet get = new HttpGet(url);

			get.setHeader("User-Agent", Utils.getUserAgent());
			get.setHeader("Connection","Keep-Alive");
			//nnection: Keep-Alive
			HttpResponse response = client.execute(get);
			
			int len = (int) response.getEntity().getContentLength();
			
			BytesResult br = new BytesResult();
			
			byte[] b = br.handleResponse(response);
		
			if(b!=null && b.length!=len)
			{
				return null;
			}
			
			return b;

		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}
	
	public static String StringFromUrl(String url)
	{

		try
		{
			
			HttpClient client = HttpClientFactory.getThreadSafeClient();
			HttpGet get = new HttpGet(url);

			HttpResponse response = client.execute(get);

			return IOUtils.toString(response.getEntity().getContent());


		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static Bitmap BitmapFromUrl(String url)
	{

		try
		{
			
			byte[] b = BytesFromUrl(url);
			
			if(b!=null)
			{
				

				BitmapFactory.Options options_bound = new Options();
				options_bound.inJustDecodeBounds = true;

				
				BitmapFactory.decodeByteArray(b, 0, b.length,
						options_bound);

				int w = options_bound.outWidth, h = options_bound.outHeight;

				
				int inSample = 1;
				
				if(w > 1800 && h > 900
						|| h > 1800 && w > 900) {
					h /= 2;
					w /= 2;
					inSample *= 2;
				}
				
				Options op = Utils.getOptions();
				op.inSampleSize = inSample;
				
				return BitmapFactory.decodeByteArray(b,0,b.length,op);
				
				
			}
			
			return null;
			
			
			//return ImageHelper.DownloadImage(url);
			
			
			/*
			 
			url = VovaUtils.encodeUrl(url);

			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(url);
			get.setHeader("User-Agent", ProjectRelations.getUserAgent());
			
			HttpResponse response = client.execute(get);
			
			
			return BitmapFactory.decodeStream(response.getEntity().getContent(), null,
							Utils.getOptions());
							
			*/

		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static interface IFileNameGetter
	{
		String getFileName(String url);
	}

	static IFileNameGetter file_namer = new IFileNameGetter()
	{

		@Override
		public String getFileName(String url)
		{

			String fileName;

			try
			{
				fileName = makeSHA1Hash(url);
			} catch (Exception e)
			{
				fileName = "" + url.hashCode();
			}

			return fileName;
		}
	};

	public static String makeSHA1Hash(String input)
			throws NoSuchAlgorithmException
	{
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.reset();
		byte[] buffer = input.getBytes();
		md.update(buffer);
		byte[] digest = md.digest();

		String hexStr = "";
		for (int i = 0; i < digest.length; i++)
		{
			hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16)
					.substring(1);
		}
		return hexStr;
	}

	public static class BytesResult implements ResponseHandler<byte[]> {
		public byte[] handleResponse(HttpResponse aHttpresponse) throws ClientProtocolException, IOException {
			final InputStream contentStream = aHttpresponse.getEntity().getContent();
			return Read(contentStream);
		}
	}
	
	static public byte[] Read(InputStream contentStream) throws ClientProtocolException, IOException {
		
		final BufferedInputStream in = new BufferedInputStream(contentStream, IO_BUFFER_SIZE);
		final ByteArrayBuffer buffer = new ByteArrayBuffer(200);
		
		byte[] current = new byte[100];
		int lengthRead;
		while((lengthRead = in.read(current)) != -1){
			if(Thread.interrupted()) {
				throw new InterruptedIOException();
			}
			buffer.append(current, 0, lengthRead);
		}

		in.close();
		
		return buffer.toByteArray();
	}
	

}
