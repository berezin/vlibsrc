package com.utils;

public class Timeline
{

	public static boolean IS_TOOSLOW = false;
	
	public float time_interval;
	
	public float max_value;
	
	Long last;
	
	
	public Timeline(float time_interval, float max_value)
	{
		this.time_interval = time_interval;
		
		if(IS_TOOSLOW)
		{
			this.time_interval *= 2;
		}
		
		this.max_value = max_value;
	}
	
	public void reset()
	{
		value = 0;
		start_time = null;
		last = null;
	}
	
	float value = 0f;
	
	Long start_time;
	
	public float getValue()
	{
		
		if(start_time == null)
		{
			start_time = System.currentTimeMillis();
		}
		
		return value;
	}
	
	public void step(long delay){
		
		step();

	}
	
	public void step(){
		
		long delay = last!=null?System.currentTimeMillis() - last:0L;
		
		
		if(delay > time_interval)
		{
			
			delay = 0L;
			
			if(last!=null)
			{
				start_time = System.currentTimeMillis() - (last - start_time);
				last = System.currentTimeMillis();
			}
			
			
		}
		
		
		if(start_time == null)
		{
			start_time = System.currentTimeMillis();
		}
		
		value = ((float)
				(System.currentTimeMillis() - start_time) * max_value
				/ time_interval);
		
		if(value > max_value)
		{
			value = max_value;
		}
		
		last = System.currentTimeMillis();
		
	}
	
	public boolean isEnd(){
		
		return value >= max_value;
		
	}
}
