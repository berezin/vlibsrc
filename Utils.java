package com.utils;

import java.io.IOException;
import java.io.Serializable;

import com.utils.io.Base64;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;

public class Utils
{
	
	private static String DB_FOLDER = null;
	
	public static String DB_EXTERNAL_PATH(){
		
		if(DB_FOLDER==null)
		{
			DB_FOLDER = App.get().getPackageName();
		}
		
		return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)?Environment.getExternalStorageDirectory().toString()+"/Android/data/"+DB_FOLDER+"/files/":App.get().getCacheDir().getPath();
	}
	
	public static int getReadTimeout()
	{
		return 15000;
	}
	public static int getConnectTimeout()
	{
		return 15000;
	}
	
	public static boolean IsPortrait()
	{
		
		DisplayMetrics dm = App.get().getResources().getDisplayMetrics();
		return dm.heightPixels > dm.widthPixels;
	}
	
	public static int getOsVersion() {
		return Build.VERSION.SDK_INT;
	}
	
	static Boolean is_fablet;
	public static boolean IsMiniTablet()
	{
		if(is_fablet==null)
		{
			DisplayMetrics dm = App.get().getResources().getDisplayMetrics();
	        
		    double d = Math.pow(Math.pow(dm.widthPixels,2)+Math.pow(dm.heightPixels,2), 0.5)/ dm.densityDpi;

		    if(d >= 5 && d < 7)
		    	is_fablet =  true;
		    else
		    	is_fablet = false;
		}
		
		return is_fablet;
		
	}
	
	
	static Boolean is_phone;
	public static boolean IsPhone()
	{
		
		if(is_phone==null)
		{
			DisplayMetrics dm = App.get().getResources().getDisplayMetrics();
	        
		    double d = Math.pow(Math.pow(dm.widthPixels,2)+Math.pow(dm.heightPixels,2), 0.5)/ dm.densityDpi;
		   
		    if(d >= 5)
		    	is_phone = false;
		    else
		    	is_phone = true;
		}
		
		return is_phone;
	}
	
	

	
	@SuppressWarnings("deprecation")
	public static Options getOptions()
	{
		Options options = new Options();
        
		options.inPreferredConfig = Config.ARGB_8888;
		//options.inBitmap = bitmap_cache; 
        options.inDither=true;  //Disable Dithering mode
        
        //if(!ProjectRelations.isBlackberry())
        options.inPurgeable=true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
        options.inInputShareable=true;  
        
        //Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future
        //options.inTempStorage = getCurrent();
       
        
        return options;
	}
	
	static String ToString(byte[] bytes)
	{
		return Base64.encodeBytes(bytes);
	}
	
	static byte[] ToBytes(String source)
	{
		try {
			return Base64.decode(source);
		} catch (IOException e) {
			return new byte[0];
		}
	}
	 
	public static String ObjectToString(Object o)
	{

		try 
		{
			return o==null? "": Base64.encodeObject((Serializable) o);
		}
		catch (Exception e) 
		{
			return null;
		}
		
	}
	
	public static Object StringToObject(String str) 
	{
		try 
		{
			return str.equals("")?null:  Base64.decodeToObject(str);
			
		}
		catch (Exception e) {
			return null;
		} 
		
	}
	

	@SuppressWarnings("static-access")
	public static boolean IsWifi()
	{
		ConnectivityManager connManager = (ConnectivityManager) App.get().getSystemService(App.get().CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		return mWifi.isConnected();
	}
	
	public static int getAppVersionCode() {
		final String packageName = App.get().getPackageName();
		PackageInfo pInfo;
		try
		{
			pInfo = App.get().getPackageManager().getPackageInfo(packageName, 0);
			return pInfo.versionCode;
		}
		catch (Exception e)
		{
			
			e.printStackTrace();
		}
		
		return 1;
	}
	
	public static String getAppVersionName() {
		String version = "";

		final String packageName = App.get().getPackageName();
		try {
			final PackageInfo info = App.get().getPackageManager().getPackageInfo(packageName, 0);
			version = info.versionName;
		} catch(NameNotFoundException e) {
			
		}
		
		
		

		return version;
	}
	
	static String ua = "";
	public static String getUserAgent()
	{
		return ua;
	}
	public static void setUserAgent(String ua)
	{
		Utils.ua = ua;
	}
	
	
}