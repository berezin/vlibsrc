package com.utils;

import android.app.Application;

/**
 * Created by berezin-ds on 02.06.2015.
 */
public class App
{

    private static Application app;

    public static Application get()
    {
        return App.app;
    }

    public static void set(Application app)
    {
        App.app = app;
    }
}
