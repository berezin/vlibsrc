package com.utils;

/**
 * Created by berezin-ds on 14.07.2015.
 */
public class MathUtils {

    public static float getR(float target_x, float target_y, float center_x, float center_y) {

        return (float) Math.sqrt(Math.pow(target_x - center_x, 2) + Math.pow(target_y - center_y, 2));
    }

    public static float getX(float angle, float r)
    {
        return ((Double) (r * Math.cos(Math.toRadians(angle)))).floatValue();
    }

    public static float getY(float angle, float r)
    {
        return ((Double) (r * Math.sin(Math.toRadians(angle)))).floatValue();
    }

    public static float getAngle(float target_x, float target_y, float center_x, float center_y)
    {

        float angle = (float) Math.toDegrees(Math.atan2(target_y - center_y, target_x - center_x));

        if (angle < 0) {
            angle += 360;
        }

        return angle;
    }

}
