package com.utils.ui;

import com.utils.shorts.LPR;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.provider.CalendarContract.Colors;
import android.view.View;
import android.view.ViewGroup;

public class ViewLayout extends View
{

	private Integer color;
	
	public ViewLayout(Context context)
	{
		super(context);
		
	}
	
	LPR lpr;
	public LPR getLPR() {
		
		if(lpr == null) {
			lpr = LPR.create().forView(this);
		}
		
		return lpr;
	}
	
	public LPR getLPR(ViewGroup parent) {
		
		return getLPR().parent(parent);
	}
	
	@Override
	public void setBackgroundColor(int color)
	{
		if(Color.alpha(color)==0) {
			this.color = null;
		}
		else {
			this.color = color;
		}
		
		invalidate();
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{
		if(hasSuperDraw) {
			super.onDraw(canvas);
		}
		
		if(color!=null) {
			canvas.drawColor(color);
		}
	}
	
	boolean hasSuperDraw = false;
	@SuppressWarnings("deprecation")
	public void setDrawable(Drawable d)
	{
		hasSuperDraw = true;
		setBackgroundDrawable(d);
	}

}
