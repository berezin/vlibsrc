package com.utils.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View.OnClickListener;

public interface IImage
{

	public void Set(Bitmap b, Boolean is_crop);
	
	public void Clear();
	
	public void setTag(Object tag);
	
	public Object getTag();

	public void onAsyncSet(Bitmap bitmap);

	public void onAnimate();

	public Context getContext();

	public void setOnClickListener(OnClickListener listener);
}
