package com.utils.ui;

import com.utils.shorts.LPR;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.RelativeLayout;

public class ViewRelativeLayout extends RelativeLayout
{

	private Integer color;
	
	public ViewRelativeLayout(Context context)
	{
		super(context);
		
	}
	
	LPR lpr;
	public LPR getLPR() {
		
		if(lpr == null) {
			lpr = LPR.create().forView(this);
		}
		
		return lpr;
	}
	
	
	@Override
	public void setBackgroundColor(int color)
	{

		this.color = color;
		invalidate();
	}
	
	
	
	protected void dispatchDrawBackground(Canvas canvas)
	{
		if(color!=null) {
			canvas.drawColor(color);
		}

	}
	
	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		dispatchDrawBackground(canvas);
		
		super.dispatchDraw(canvas);
	}
	
	
	boolean hasSuperDraw = false;
	@SuppressWarnings("deprecation")
	public void setDrawable(Drawable d)
	{
		hasSuperDraw = true;
		setBackgroundDrawable(d);
	}

}
