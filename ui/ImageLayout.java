package com.utils.ui;

import java.util.ArrayList;
import java.util.HashMap;

import com.utils.CanvasUtils;
import com.utils.Utils;
import com.utils.image.Const;
import com.utils.shorts.A;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;

public class ImageLayout extends ViewLayout implements IImage
{

	public static Boolean TEST_IMAGEVIEW = false;
	protected Boolean is_crop = true;

	public static interface ISetImage
	{
		void post(Bitmap bitmap);
	}

	public void onAnimate()
	{
		if(isAnimating() && Utils.getOsVersion() > 15)
		{
			A.fadein(this, 300, 0.5f, null);
		}
	}

	public void addCallback(ISetImage callback)
	{
		callbacks.add(callback);
	}

	public void addAsyncCallback(ISetImage callback)
	{
		callbacks_async.add(callback);
	}

	ArrayList<ISetImage> callbacks = new ArrayList<ImageLayout.ISetImage>();
	ArrayList<ISetImage> callbacks_async = new ArrayList<ImageLayout.ISetImage>();

	public boolean is_filter = true;
	public Boolean is_xfer;
	public boolean is_filter_placeholder = true;

	ImageView image;

	void initVovaImageView()
	{

		setBackgroundColor(Const.COLOR_PLACEHOLDER);

		if(TEST_IMAGEVIEW)
		{
			image = new ImageView(getContext());
		}

	}

	public ImageLayout(Context context)
	{
		super(context);

		initVovaImageView();
	}

	public boolean isXferSrc()
	{
		if(is_xfer != null)
		{
			return is_xfer;
		}

		return Const.IS_DRAW_IMAGE_XFERMODESRC;
	}

	private boolean is_first_measuring = true;
	public boolean is_measuring_save_state = true;
	int w, h;

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		if(!is_measuring_save_state)
		{
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);

			return;
		}

		if(!is_first_measuring)
		{

			setMeasuredDimension(w, h);
		} else
		{

			super.onMeasure(widthMeasureSpec, heightMeasureSpec);

			w = getMeasuredWidth();
			h = getMeasuredHeight();

		}

	}

	public void removePlaceholder()
	{
		need_placeholder = false;
		setBackgroundColor(0x00000000);
	}

	public boolean need_placeholder = true;

	public boolean is_attached = false;

	public boolean is_clear_on_detach = true;

	@Override
	protected void onDetachedFromWindow()
	{

		super.onDetachedFromWindow();

		is_attached = false;

		if(is_clear_on_detach)
		{
			Clear();

			rect_img_placeholder = null;
			rect_img_placeholder_for_drawing = null;

			rect_img_for_drawing = null;

			callbacks.clear();
			callbacks_async.clear();

			setTag("");
		}

	}

	@Override
	protected void onAttachedToWindow()
	{
		// TODO Auto-generated method stub
		super.onAttachedToWindow();

		// BAssert.log("problem onAttachedToWindow");

		clearAnimation();

		is_attached = true;
	}

	public boolean isAnimating()
	{

		return true;
	}

	Rect rectSrc;

	protected Rect rect_img = new Rect();

	protected Rect rect_img_placeholder = null;

	protected Rect rect_img_for_drawing = null;

	protected Rect rect_img_placeholder_for_drawing = null;

	protected static Rect rectSrcPlaceholder;

	public void drawPlaceholder(Canvas canvas)
	{

		if(Const.PLACEHOLDER == null)
		{
			return;
		}

		if(rectSrcPlaceholder == null)
		{
			rectSrcPlaceholder = new Rect(0, 0, Const.PLACEHOLDER.getWidth(),
					Const.PLACEHOLDER.getHeight());
		}

		if(rect_img_placeholder_for_drawing != null)
		{
			CanvasUtils.DrawImage(canvas, Const.PLACEHOLDER,
					rect_img_placeholder, rect_img_placeholder_for_drawing,
					rectSrcPlaceholder, is_filter_placeholder, 255, false);
		} else
		{
			int w = getMeasuredWidth();
			int h = getMeasuredHeight();

			int wp = (int) (w * Const.PLACEHOLDER_PADDING_PERCENT);

			rect_img_placeholder = new Rect(wp, 0, w - wp, h);

			rect_img_placeholder_for_drawing = CanvasUtils.DrawImage(canvas,
					Const.PLACEHOLDER, rect_img_placeholder, false,
					is_filter_placeholder, 255, false);
		}

	}

	@Override
	public void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
		
		if(TEST_IMAGEVIEW)
		{
			return;
		}

		if(getMeasuredWidth() > 0 && getMeasuredHeight() > 0)
		{

		} else
		{
			return;
		}

		if(bitmap != null)
		{

			if(rect_img_for_drawing == null)
			{
				rect_img.right = getMeasuredWidth();
				rect_img.bottom = getMeasuredHeight();

				rect_img_for_drawing = CanvasUtils.DrawImage(canvas, bitmap,
						rect_img, is_crop, is_filter, 255, isXferSrc());

			} else
			{
				CanvasUtils.DrawImage(canvas, bitmap, rect_img,
						rect_img_for_drawing, rectSrc, is_filter, 255,
						isXferSrc());
			}

		} else
		{

			super.dispatchDraw(canvas);

			if(need_placeholder)
			{
				drawPlaceholder(canvas);
			}

		}

	}

	// public Bitmap bitmap;

	public Bitmap bitmap;

	public void onAsyncSet(Bitmap b)
	{

		for (ISetImage callback : callbacks_async)
		{
			callback.post(b);
		}
	}

	public void Set(Bitmap b, Boolean is_crop)
	{

		if(b == bitmap)
		{
			return;
		}

		bitmap = b;

		if(TEST_IMAGEVIEW)
		{
			image.setImageBitmap(bitmap);
			image.setScaleType(
					is_crop ? ScaleType.CENTER_CROP : ScaleType.FIT_CENTER);
		}

		if(Looper.getMainLooper() != Looper.myLooper())
		{
			post(new Runnable()
			{

				@Override
				public void run()
				{
					if(bitmap != null && !bitmap.isRecycled())
						for (ISetImage callback : callbacks)
						{
							callback.post(bitmap);
						}
				}
			});
		} else
		{
			for (ISetImage callback : callbacks)
			{
				callback.post(bitmap);
			}
		}

		rectSrc = new Rect(0, 0, b.getWidth(), b.getHeight());

		setTag("");

		rect_img_for_drawing = null;

		// bitmap = b;

		this.is_crop = is_crop;

		// invalidate();

		if(Looper.getMainLooper() == Looper.myLooper())
		{
			invalidate();
		} else
		{
			postInvalidate();
		}

	}

	public void Clear()
	{

		bitmap = null;

		if(TEST_IMAGEVIEW)
		{
			image.setImageBitmap(null);
		}
		// callbacks.clear();

		setClickable(false);
		
		rect_img_for_drawing = null;

		setTag("");

		clearAnimation();

		invalidate();

	}

	public interface OnVovaBitmapLoaded
	{
		Bitmap post(ImageLayout imageView, Bitmap bitmap,
				boolean is_another_imageview);

		void onError();

		void onTry();
	}

	public boolean isHasTag()
	{

		return getTag() != null && getTag().toString().length() > 0;
	}

	public void setDefaultImageView()
	{
		is_xfer = false;
		removePlaceholder();
		is_clear_on_detach = false;
	}

	public void Set(Integer ic, boolean is_crop)
	{
		Set(BitmapFactory.decodeResource(getResources(), ic), is_crop);
	}

}
