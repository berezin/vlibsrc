package com.utils.ui;

import com.utils.App;
import com.utils.shorts.LPR;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;

public class TextLayout extends ViewLayout
{

	StaticLayout sl;
	Integer width;
	
	public TextLayout() {
		super(App.get());
	}
	
	public TextLayout(Context context) {
		super(context);
	}
	
	public void set(CharSequence str, int width, TextPaint paint, Alignment align) {
		this.width = width;
		sl = new StaticLayout(str, paint, width, align, 1f, 0f, false);
	}
	
	public void set(CharSequence str, int width, TextPaint paint) {
		this.width = width;
		sl = new StaticLayout(str, paint, width, Alignment.ALIGN_NORMAL, 1f, 0f, false);
	}
	
	public void set(CharSequence str, TextPaint paint) {
		sl = new StaticLayout(str, paint, Integer.MAX_VALUE, Alignment.ALIGN_NORMAL, 1f, 0f, false);
		width = (int) sl.getLineWidth(0);
	}
	
	public TextLayout(Context context, CharSequence str, int width, TextPaint paint, Alignment align)
	{
		super(context);
		set(str, width, paint, align);
	}
	
	public TextLayout(Context context, CharSequence str, int width, TextPaint paint)
	{
		this(context, str, width, paint, Alignment.ALIGN_NORMAL);
	}
	
	public TextLayout(Context context, CharSequence str, TextPaint paint)
	{
		super(context);
		set(str, paint);
	}
	
	
	public LPR getLPR() {
		
		if(lpr == null) {
			lpr = LPR.create(Width(), Height()).forView(this);
		}
		
		return lpr;
	}
	
	
	public void setPadding(int pad) {
		setPadding(pad, pad, pad, pad);
	}
	
	boolean hasPadding = false;
	int padTop = 0;
	int padBottom = 0;
	int padLeft = 0;
	int padRight = 0;
	
	@Override
	public void setPadding(int left, int top, int right, int bottom)
	{
		hasPadding = true;
		padTop = top;
		padBottom = bottom;
		padLeft = left;
		padRight = right;
	}
	
	public int Height() {
		return sl.getHeight() + padTop + padBottom;
	}
	
	public int Width() {
		return width + padLeft + padRight;
	}
	
	
	
	@Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
		
		if(sl!=null) {
			
			if(hasPadding) {
				canvas.save();
				canvas.translate(padLeft, padTop);
			}
			
			sl.draw(canvas);
			
			if(hasPadding) {
				canvas.restore();
			}
		}
	}

	
}
