package com.utils;

import java.util.LinkedList;
import java.util.List;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.provider.Settings.Secure;

public class Device
{

	public static String ID()
	{
		return Secure.getString(App.get().getContentResolver(), Secure.ANDROID_ID);
	}
	
	public static String getUsername()
	{
		try
		{
			AccountManager manager = AccountManager.get(App.get());
			Account[] accounts = manager.getAccountsByType("com.google");
			Account[] accounts2 = manager.getAccountsByType("ru.yandex");
			List<String> possibleEmails = new LinkedList<String>();
			
			if(accounts!=null)
			for (Account account : accounts)
			{
				// TODO: Check possibleEmail against an email regex or treat
				// account.name as an email address only for certain account.type
				// values.
				possibleEmails.add(account.name);
			}
			if(accounts2!=null)
			for (Account account : accounts2)
			{
				// TODO: Check possibleEmail against an email regex or treat
				// account.name as an email address only for certain account.type
				// values.
				possibleEmails.add(account.name);
			}

			if(!possibleEmails.isEmpty() && possibleEmails.get(0) != null)
			{
				String email = possibleEmails.get(0);
				String[] parts = email.split("@");
				if(parts.length > 0 && parts[0] != null)
					return parts[0];
				else
					return null;
			} else
				return null;
		} catch (Exception e)
		{
			
			e.printStackTrace();
		}
		
		return null;
	}
}
