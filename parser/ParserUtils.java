package com.utils.parser;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.utils.Utils;
import com.utils.io.IO.HttpClientFactory;

import android.util.Log;

public class ParserUtils
{

	public static final int IO_BUFFER_SIZE = 8 * 1024;

	public static class ParseField
	{
		Field f;
		Annotation[] anns;
		@SuppressWarnings("rawtypes")
		Class[] cls;
		boolean isList = false;

		public ParseField(Field f, Annotation[] anns)
		{
			this.f = f;
			this.anns = anns;

			if(anns != null)
			{
				cls = new Class[anns.length];

				for (int i = 0; i < anns.length; i++)
				{
					cls[i] = anns[i].annotationType();
				}
			}

			isList = f.getType() == ArrayList.class;
		}

		@SuppressWarnings("rawtypes")
		public Annotation getAnnotation(Class cl)
		{
			if(cls == null)
				return null;

			for (int i = 0; i < cls.length; i++)
			{
				if(cl.equals(cls[i]))
					return anns[i];
			}

			return null;
		}
	}

	public static class O
	{
		static final String START_TAG = "tag_tag_tag";

		@SuppressWarnings("rawtypes")
		public Class cl;
		public Object object;
		public String tag;
		public int openedTag = 1;
		public String cur_tag = START_TAG;

		O(Object object, String tag)
		{
			super();
			this.cl = object.getClass();
			this.object = object;
			this.tag = tag;
		}

		@Override
		public String toString()
		{

			try
			{
				return "" + cl + " " + object + " " + object.getClass() + " "
						+ tag + " " + cur_tag;
			} catch (Exception e)
			{
				return "";
			}
		}

		@SuppressWarnings("rawtypes")
		static HashMap<Class, HashMap<String, ParserUtils.ParseField>> class_fields = new HashMap<>();
		@SuppressWarnings("rawtypes")
		static HashMap<Class, ArrayList<Field>> map_fields = new HashMap<>();
		

		public static ParserUtils.ParseField find(ParserUtils.O o)
		{

			String str = o.cur_tag;
			HashMap<String, ParserUtils.ParseField> fields = class_fields
					.get(o.cl);

			
			
			if(fields == null)
			{
				fields = new HashMap<String, ParserUtils.ParseField>();
				class_fields.put(o.cl, fields);
			}

			if(fields.containsKey(str))
			{
				return fields.get(str);
			}
			
			ArrayList<Field> usedFields = map_fields.get(o.cl);
			
			if(usedFields == null)
			{
				usedFields = new ArrayList<>();
				map_fields.put(o.cl, usedFields);
			}
			

			Field[] ff = o.cl.getFields();
			String name = "";
			
			for (Field f : ff)
			{

				if(usedFields.contains(f)) {
					continue;
				}
				
				name = "-1";

				f.setAccessible(true);
				Annotation[] anns = f.getAnnotations();

				ParserUtils.ParseField pf = new ParseField(f, anns);

				Annotation ann;

				if((ann = pf.getAnnotation(ParserUtils.Data.class)) != null)
				{
					name = ((ParserUtils.Data) ann).name();
				} else if((ann = pf
						.getAnnotation(ParserUtils.DList.class)) != null)
				{
					name = ((ParserUtils.DList) ann).name();
				} else
				{
					continue;
				}

				if(name.equals("_"))
				{
					name = f.getName();
				}

				if(str.equals(name))
				{
					fields.put(str, pf);
					usedFields.add(f);
					return pf;
				}
			}

			fields.put(str, null);

			return null;
		}

	}

	@Target(value = ElementType.FIELD)
	@Retention(value = RetentionPolicy.RUNTIME)
	static public @interface DataNoBase
	{
	}

	@Target(value = ElementType.FIELD)
	@Retention(value = RetentionPolicy.RUNTIME)
	static public @interface DataName
	{
		String fieldname();
	}

	@Target(value = ElementType.FIELD)
	@Retention(value = RetentionPolicy.RUNTIME)
	static public @interface Data
	{
		String name() default "_";

		String type() default "varchar(100)";
	}

	@Target(value = ElementType.FIELD)
	@Retention(value = RetentionPolicy.RUNTIME)
	static public @interface DList
	{
		String name() default "_";
	}
	
	

	public static class JSONParser
	{

		Stack<ParserUtils.O> stack = new Stack<ParserUtils.O>();

		boolean bool_node = false;
		Field field;

		public static boolean Parse(Object obj, String uri, int connect_timeout,
				int readtimeout)
		{
			try
			{
				URL url = new URL(uri);
				URLConnection cn = url.openConnection();
				cn.setConnectTimeout(connect_timeout);
				cn.setReadTimeout(readtimeout);
				cn.setRequestProperty("User-Agent", Utils.getUserAgent());
				cn.connect();

				final BufferedInputStream stream = new BufferedInputStream(
						cn.getInputStream(), IO_BUFFER_SIZE);

				Parse(obj, stream);
				return true;
			} catch (Exception e)
			{
				e.printStackTrace();

			}
			return false;

		}

		public static class Post {
			
			HashMap<String, Object> map = new HashMap<>();
			
			public static Post create() {
				return new Post();
			}
			
			public static Post create(String key, Object value) {
				
				Post p = new Post();
				p.add(key, value);
				return p;
			}
			
			public Post add(String key, Object value) {
				
				map.put(key, value);
				return this;
			}
			
			public HashMap<String, Object> get() {

				return map;
			}
			
		}
		
		public static boolean ParsePost(Object obj, String url, HashMap<String, Object> map)
		{
			
			try
			{
				
				JSONObject json = new JSONObject();
				for(Entry<String, Object> entry: map.entrySet()) {
					json.put(entry.getKey(), entry.getValue());
				}
				
				return ParsePost(obj, url, json);
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			return false;
		}
		
		public static boolean ParsePut(Object obj, String url, HashMap<String, Object> map)
		{
			
			try
			{
				
				JSONObject json = new JSONObject();
				for(Entry<String, Object> entry: map.entrySet()) {
					json.put(entry.getKey(), entry.getValue());
				}
				
				return ParsePut(obj, url, json);
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			return false;
		}
		
		@SuppressWarnings({ "deprecation", "resource" })
		public static boolean ParsePost(Object obj, String url, JSONObject json)
		{
			try
			{
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(url);
				StringEntity se = new StringEntity(json.toString(), "UTF-8");
				se.setContentType(
						new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
				post.setHeader("Accept", "application/json");
				post.setHeader("Content-type", "application/json");
				post.setEntity(se);
				HttpResponse response = client.execute(post);

				return Parse(obj, response.getEntity().getContent());
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			return false;
		}
		
		@SuppressWarnings({ "deprecation", "resource" })
		public static boolean ParsePut(Object obj, String url, JSONObject json)
		{
			try
			{
				HttpClient client = new DefaultHttpClient();
				HttpPut post = new HttpPut(url);
				StringEntity se = new StringEntity(json.toString(), "UTF-8");
				se.setContentType(
						new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
				post.setHeader("Accept", "application/json");
				post.setHeader("Content-type", "application/json");
				post.setEntity(se);
				HttpResponse response = client.execute(post);

				return Parse(obj, response.getEntity().getContent());
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			return false;
		}
		
		public static boolean ParseText(Object obj, String text)
		{
			try
			{
				InputStream is = new ByteArrayInputStream(
						text.getBytes("UTF-8"));

				return Parse(obj, is);
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			return false;
		}

		public static boolean ParseFile(Object obj, String path)
		{
			try
			{
				// FileUtils.readFileToByteArray(file)//readFileToString(new
				// File(path), "UTF-8")
				InputStream is = FileUtils.openInputStream(new File(path));// new
																			// ByteArrayInputStream(
																			// Utils.getStringFromFile(path).getBytes(
																			// "UTF-8"
																			// )
																			// );

				return Parse(obj, is);
			} catch (Exception e)
			{
				e.printStackTrace();

			}

			return false;

		}

		public static boolean Parse(Object obj, String uri)
		{
			// BAssert.log(uri);
			try
			{
				/*
				 * URL url = new URL(uri); URLConnection cn =
				 * url.openConnection();
				 * cn.setConnectTimeout(Utils.getConnectTimeout());
				 * cn.setReadTimeout(Utils.getReadTimeout());
				 * cn.setRequestProperty("User"+"-Agent", Utils.getUserAgent());
				 * cn.connect(); final BufferedInputStream stream = new
				 * BufferedInputStream(cn.getInputStream(),
				 * IoUtils.IO_BUFFER_SIZE);
				 */

				HttpClient client = HttpClientFactory.getThreadSafeClient();// new
																			// DefaultHttpClient();
				HttpGet get = new HttpGet(uri);
				HttpResponse response = client.execute(get);
				InputStream stream = response.getEntity().getContent();

				return Parse(obj, stream);
				// return true;
			} catch (Exception e)
			{
				e.printStackTrace();

			}
			return false;

		}

		public static boolean Parse(Object obj, InputStream is)
		{

			try
			{
				new JSONParser(obj, is);
				return true;
			} catch (Exception e)
			{

				e.printStackTrace();
			}

			return false;
		}

		private JSONParser(Object obj, InputStream stream)
		{
			try
			{
				
				if(obj == null) {
					obj = new Object();
				}
				
				JsonReader reader = new JsonReader(
						new InputStreamReader(stream, "UTF-8"));

				stack.push(new O(obj, O.START_TAG));

				prettyprint(reader);

				reader.close();

			} catch (Exception e)
			{
				Log.w("Parser", "" + e.getMessage());
			}
		}

		@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
		void prettyprint(JsonReader reader) throws IOException
		{

			while (true)
			{
				JsonToken token = reader.peek();
				switch (token)
				{

					case BEGIN_ARRAY:
						reader.beginArray();
						// BAssert.log("BEGIN_ARRAY");
						// stack.peek().openedTag++;
						break;
					case END_ARRAY:
						reader.endArray();
						// BAssert.log("END_ARRAY");
						// stack.peek().openedTag--;
						break;
					case BEGIN_OBJECT:
						reader.beginObject();
						// BAssert.log("BEGIN_OBJECT");

						if(stack.peek().cur_tag.equals(O.START_TAG))
							break;

						stack.peek().openedTag++;
						String name = stack.peek().cur_tag;
						ParserUtils.ParseField pf = O.find(stack.peek());

						if(pf != null)
						{

							Field f = pf.f;
							f.setAccessible(true);

							if(pf.getAnnotation(ParserUtils.Data.class) != null)
							{

								Object obj = null;
								try
								{
									if(pf.isList)
									{

										try
										{

											
											ParameterizedType superclass = (ParameterizedType) f
													.getGenericType();
											Class cl = (Class) superclass
													.getActualTypeArguments()[0];

											if(f.get(stack
													.peek().object) == null)
											{
												f.set(stack.peek().object,
														f.getType()
																.newInstance());
											}

											obj = cl.newInstance();

											if(obj == null)
											{
												Log.w("Parser", "Null create: "
														+ cl.getName());
											} else
											{
												((List) (f.get(
														stack.peek().object)))
																.add(obj);
											}

										} catch (Exception e)
										{
											Log.w("Parser", "datalist error: "
													+ e.getMessage());
										}

									} else
									{

										try
										{

											obj = f.getType().newInstance();

											if(obj == null)
											{
												Log.w("Parser",
														"Null create: " + f
																.getType()
																.getName());
											}

											f.set(stack.peek().object, obj);

										} catch (Exception e)
										{
											Log.w("Parser", "data error: "
													+ e.getMessage());
										}

									}

									if(obj != null)
									{

										stack.push(new O(obj,
												stack.peek().cur_tag));

									}

								} catch (Exception e)
								{
									Log.w("Parser",
											"Data error: " + e.getMessage());
								}

							}
						}
						
						break;
					case END_OBJECT:
						reader.endObject();
						// BAssert.log("END_OBJECT");
						try
						{

							if(stack.peek().cur_tag.equals(O.START_TAG))
								break;

							stack.peek().openedTag--;

							if(stack.peek().openedTag == 0)
							{
								stack.pop();
								if(stack.size() != 0)
									stack.peek().openedTag--;

							}
						} catch (Exception e)
						{
							Log.w("Parser", "ENDO error: " + e.getMessage());
						}

						break;
					case NAME:

						stack.peek().cur_tag = reader.nextName();
						// BAssert.log( stack.peek().cur_tag);

						break;
					case STRING:
						ValueObject(reader, String.class);
						// Value(reader.nextString());
						break;
					case NUMBER:
						ValueObject(reader, Double.class);
						// Value(reader.nextString());
						break;
					case BOOLEAN:

						ValueObject(reader, Boolean.class);
						// Value(""+reader.nextBoolean());
						break;
					case NULL:
						reader.nextNull();

						break;
					case END_DOCUMENT:
						return;
				}
			}
		}

		@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
		void ValueObject(JsonReader reader, Class cl_value)
		{
			// BAssert.log(val);
			boolean is_readed = false;

			String name = stack.peek().cur_tag;
			ParserUtils.ParseField pf = O.find(stack.peek());
			if(pf != null)
			{
				pf.f.setAccessible(true);

				Annotation ann;

				if((ann = pf.getAnnotation(ParserUtils.Data.class)) != null)
				{

					if(!pf.isList)
					{

						try
						{
							Class cl = pf.f.getType();

							Object o = stack.peek().object;

							is_readed = true;

							if(cl == String.class)
							{

								String v;

								if(cl_value == Boolean.class)
								{
									v = "" + reader.nextBoolean();
								} else
								{
									v = reader.nextString();
								}

								try
								{
									pf.f.set(o, v);
								} catch (Exception e)
								{
									e.printStackTrace();
								}

							} else if(cl == Integer.class)
							{

								Integer v = reader.nextInt();

								try
								{
									pf.f.set(o, v);
								} catch (Exception e)
								{
									e.printStackTrace();
								}

							} else if(cl == Double.class)
							{

								Double v = reader.nextDouble();

								try
								{
									pf.f.set(o, v);
								} catch (Exception e)
								{
									e.printStackTrace();
								}

							} else if(cl == Long.class)
							{
								Long v = reader.nextLong();

								try
								{
									pf.f.set(o, v);
								} catch (Exception e)
								{
									e.printStackTrace();
								}

							} else if(cl == Boolean.class)
							{

								Boolean v = reader.nextBoolean();

								try
								{
									pf.f.set(o, v);
								} catch (Exception e)
								{
									e.printStackTrace();
								}

							} else
							{
								is_readed = false;

							}

						} catch (Exception e)
						{

							is_readed = false;

							e.printStackTrace();

						}
					} else
					{

						try
						{

							Object o = null;

							if((o = pf.f.get(stack.peek().object)) == null)
							{
								pf.f.set(stack.peek().object,
										(o = pf.f.getType().newInstance()));
							}
							is_readed = true;

							((List) (o)).add(reader.nextString());

						} catch (Exception e)
						{
							e.printStackTrace();
							Log.w("Parser", "list error: " + e.getMessage());
						}
					}
				}

			}

			if(!is_readed)
			{
				try
				{
					if(cl_value == Boolean.class)
					{
						reader.nextBoolean();
					} else
					{
						reader.nextString();
					}

				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}

		}

	}

	public static class XMLParser extends DefaultHandler
	{

		public static boolean Parse(Object obj, String uri)
		{

			try
			{
				URL url = new URL(uri);
				URLConnection cn = url.openConnection();
				cn.setConnectTimeout(Utils.getConnectTimeout());
				cn.setReadTimeout(Utils.getReadTimeout());
				cn.setRequestProperty("User-Agent", Utils.getUserAgent());
				cn.connect();
				final BufferedInputStream stream = new BufferedInputStream(
						cn.getInputStream(), IO_BUFFER_SIZE);

				return Parse(obj, stream);
			} catch (Exception e)
			{
				Log.w("Parser", "" + e.getMessage());
			}

			return false;
		}

		public static boolean ParseFile(Object obj, String path)
		{
			try
			{

				InputStream is = FileUtils.openInputStream(new File(path));// new
																			// ByteArrayInputStream(
																			// Utils.getStringFromFile(path).getBytes(
																			// "UTF-8"
																			// )
																			// );

				return Parse(obj, is);
			} catch (Exception e)
			{
				e.printStackTrace();

			}

			return false;

		}

		public static boolean Parse(Object obj, InputStream is)
		{

			try
			{
				SAXParser parser = SAXParserFactory.newInstance()
						.newSAXParser();

				parser.parse(is, new XMLParser(obj));

				return true;
			} catch (Exception e)
			{

				e.printStackTrace();
			}

			return false;
		}

		/*
		 * static class ParseField { Field f; Annotation[] anns; Class[] cls;
		 * //String name;
		 * 
		 * public ParseField(Field f,Annotation[] anns) { this.f =f; this.anns =
		 * anns; if(anns!=null) { cls = new Class[anns.length];
		 * 
		 * for(int i=0;i<anns.length;i++) { cls[i] = anns[i].annotationType(); }
		 * } //this.name = name; } public Annotation getAnnotation(Class cl) {
		 * if(cls==null) return null;
		 * 
		 * for(int i=0;i<cls.length;i++) { if(cl.equals(cls[i])) return anns[i];
		 * }
		 * 
		 * return null; } }
		 * 
		 * static class O { public Class cl; public Object object; public String
		 * tag; public int openedTag=1;
		 * 
		 * public O(Object object, String tag) { super(); this.cl =
		 * object.getClass(); this.object = object; this.tag = tag; }
		 * 
		 * 
		 * static HashMap<Class,HashMap<String, ParseField>> class_fields = new
		 * HashMap<Class,HashMap<String, ParseField>>();
		 * 
		 * static ParseField find(O o,String str) {
		 * 
		 * HashMap<String, ParseField> fields = class_fields.get(o.cl);
		 * 
		 * if(fields==null) {
		 * 
		 * fields = new HashMap<String, ParseField>(); class_fields.put(o.cl,
		 * fields);
		 * 
		 * }
		 * 
		 * 
		 * if(fields.containsKey(str)) return fields.get(str);
		 * 
		 * 
		 * 
		 * Field[] ff = o.cl.getFields(); String name="";
		 * 
		 * for(Field f:ff) { name = "-1";
		 * 
		 * f.setAccessible(true); Annotation[] anns = f.getAnnotations();
		 * 
		 * ParseField pf = new ParseField(f, anns);
		 * 
		 * Annotation ann;
		 * 
		 * 
		 * if((ann = pf.getAnnotation(Data.class))!=null) { name =
		 * ((Data)ann).name(); } else if((ann =
		 * pf.getAnnotation(DataList.class))!=null) { name =
		 * ((DataList)ann).name(); }
		 * 
		 * 
		 * if(name.equals("_")) { try {
		 * 
		 * name = f.getName();
		 * 
		 * if(name.equals("list")) for(Field f2:ff) {
		 * if(f2.isAnnotationPresent(DataName.class) &&
		 * f2.getAnnotation(DataName.class).fieldname().equals(f.getName())) {
		 * name = f2.get(o.object).toString(); break; } }
		 * 
		 * } catch (Exception e) { BAssert.log("Name not finded!"); }
		 * 
		 * }
		 * 
		 * if(str.equals(name)) {
		 * 
		 * if(str.equals("article")) { int i=0; }
		 * 
		 * fields.put(str, pf);
		 * 
		 * return pf; } }
		 * 
		 * fields.put(str, null);
		 * 
		 * return null; }
		 * 
		 * }
		 */

		Stack<ParserUtils.O> stack = new Stack<ParserUtils.O>();

		StringBuilder text = new StringBuilder();

		public XMLParser(Object object)
		{

			stack.push(new O(object, cur_tag));

		}

		@Override
		public void characters(char[] ch, int start, int length)
				throws SAXException
		{
			super.characters(ch, start, length);

			if(bool_node)
				text.append(ch, start, length);// new String(ch,start,length);//
												// String.valueOf(ch).substring(start,
												// start+length);

		}

		@Override
		public void endDocument() throws SAXException
		{
			super.endDocument();

		}

		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException
		{
			super.endElement(uri, localName, qName);

			stack.peek().openedTag--;

			if(bool_node)
			{
				// BAssert.log("10");

				try
				{
					field.set(stack.peek().object, text.toString());
					bool_node = false;
				} catch (Exception e)
				{
					Log.w("Parser", "Node error: " + e.getMessage());
				}
				// BAssert.log("11");
			} else if(stack
					.peek().openedTag == 0/*
											 * localName.equals(stack.peek().tag)
											 */)
			{
				stack.pop();
				stack.peek().openedTag--;
			}

		}

		@Override
		public void startDocument() throws SAXException
		{
			super.startDocument();
		}

		boolean bool_node = false;
		Field field;

		String cur_tag = "tag_tag_tag";

		@SuppressWarnings({ "rawtypes", "unchecked" })
		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException
		{

			super.startElement(uri, localName, qName, attributes);

			stack.peek().openedTag++;
			// BAssert.log("1");
			stack.peek().cur_tag = localName;
			ParserUtils.ParseField pf = O.find(stack.peek());

			// BAssert.log("2");
			ParserUtils.ParseField f2;
			if(pf != null)
			{
				Field f = pf.f;
				f.setAccessible(true);
				// BAssert.log(localName+" "+f.getName());
				/*
				 * if(f.isAnnotationPresent(DataString.class)) { bool_node =
				 * true; field = f; } else
				 */
				if(pf.getAnnotation(ParserUtils.Data.class) != null
						|| pf.getAnnotation(ParserUtils.DList.class) != null)
				{
					cur_tag = localName;

					Object obj = null;
					try
					{
						if(pf.getAnnotation(ParserUtils.DList.class) != null)
						{
							try
							{

								// BAssert.log("3");

								ParameterizedType superclass = (ParameterizedType) f
										.getGenericType();
								Class cl;
								/*
								 * if(superclass.getActualTypeArguments()[0].
								 * toString().equals("D")) { Getter g = (Getter)
								 * stack.peek().object; cl =
								 * g.getParameterClass(); } else
								 */
								{
									cl = (Class) superclass
											.getActualTypeArguments()[0];
								}

								if(f.get(stack.peek().object) == null)
								{
									f.set(stack.peek().object,
											f.getType().newInstance());
								}

								obj = cl.newInstance();

								if(obj == null)
								{
									Log.w("Parser",
											"Nill create: " + cl.getName());
								} else
									((List) (f.get(stack.peek().object)))
											.add(obj);

								// BAssert.log("4");
							} catch (Exception e)
							{
								Log.w("Parser",
										"object error: " + e.getMessage());
							}

						} else
						{

							try
							{

								obj = f.getType().newInstance();

								if(obj == null)
								{
									Log.w("Parser", "Nill create: "
											+ f.getType().getName());
								}

								f.set(stack.peek().object, obj);

							} catch (InstantiationException e)
							{
								Log.w("Parser",
										"data error: " + e.getMessage());
							}

						}

						if(obj != null)
						{

							// BAssert.log("7");

							stack.push(new O(obj, localName));

							int n = attributes.getLength();

							for (int i = 0; i < n; i++)
							{
								stack.peek().cur_tag = attributes
										.getLocalName(i);
								f2 = O.find(stack.peek());

								if(f2 != null)
								{
									try
									{
										f2.f.set(stack.peek().object,
												attributes.getValue(i));
									} catch (Exception e)
									{
										Log.w("Parser", "attribute error: "
												+ e.getMessage());
									}
								}
							}

							// BAssert.log("8");
						}

					} catch (Exception e)
					{
						Log.w("Parser", "Data error: " + e.getMessage());
					}

				}

			} else
			{

			}

			if(bool_node)
				text = new StringBuilder();
		}
	}
}