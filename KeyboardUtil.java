package com.utils;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class KeyboardUtil
{

	
	
	public static void Close(EditText edit) {
		InputMethodManager inputMethodManager = (InputMethodManager) edit.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(edit.getWindowToken(), 0);
	}
	
	public static void Open(EditText edit) {
		edit.requestFocus();
		InputMethodManager inputMethodManager = (InputMethodManager) edit.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}

}
