package com.utils;

import android.os.Looper;
import android.util.Log;

public abstract class InternalService
{
	
	WeakHandler handler;
	
	public class InternalServiceRunnable implements Runnable {

		Integer token;
		
		public void setToken(Integer token) {
			this.token = token;
		}
		
		public boolean isValid() {
			return token!= null && token.equals(InternalService.this.token);
		}
		
		@Override
		public void run()
		{

			if(is_stoped) {
				return;
			}

			if(isValid()) {
				_process();
			}
		}
		
	}
	
	public InternalService()
	{
		handler = new WeakHandler(Looper.getMainLooper());
	}
	
	abstract public int getTimeout();
	
	abstract public void process();
	
	private InternalServiceRunnable currentRunnable = new InternalServiceRunnable();
	boolean is_processing = false;
	private void _process()
	{
		if(is_stoped)
			return;
		
		
		if(!is_processing)
		{
			is_processing = true;
			
			process();
					
			handleDelayed(getTimeout());
			
			is_processing = false;

		}
		
	}
	
	private void handleDelayed(int delay) {
		
		currentRunnable.setToken(token);
		handler.postDelayed(currentRunnable, delay);	
	}
	
	int token = -1;
	public void start()
	{
		stop();
		token = (int) (Math.random()*10000);
		is_stoped = false;
		
		if(Looper.getMainLooper()!= Looper.myLooper())
		{
			handleDelayed(0);
		}
		else
		{
			_process();
		}
		 
	}
	
	
	public void start(int delay)
	{
		stop();
		
		token = (int) (Math.random()*10000);
		is_stoped = false;
		
		handleDelayed(delay);
		 
	}
	
	boolean is_stoped = true;
	public void stop()
	{
		token = -1;
		is_stoped = true;
		
		if(currentRunnable!=null) {
			handler.removeCallbacks(currentRunnable);
		}
	}
	
	public boolean IsStarted()
	{
		return !is_stoped;
	}
	

	
}
