package com.utils;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;

/**
 * Created by berezin-ds on 02.06.2015.
 */

@SuppressWarnings("unused")
public class ResourceUtil {

    public static Resources getResources()
    {
        return App.get().getResources();
    }


    public static String getStr(int id) {
        return getResources().getString(id);
    }

    public static int getColor(int id)
    {
        return getResources().getColor(id);
    }

    @SuppressWarnings("deprecation")
    public static Drawable getDrawable(int id) {

        return getResources().getDrawable(id);
    }

    public static float getDim(int id) {
        return getResources().getDimension(id);
    }

    public static int getInt(int id) {
        return getResources().getInteger(id);
    }

    public static float getScale() {
        return getResources().getDisplayMetrics().scaledDensity;
    }

    public static float getDensity() {
        return getResources().getDisplayMetrics().density;
    }

    public static Float _w;
	public static float getW()
	{
		if(_w == null) {
			float w = App.get().getResources().getDisplayMetrics().widthPixels;
			
			if(w > App.get().getResources().getDisplayMetrics().heightPixels)
				w = App.get().getResources().getDisplayMetrics().heightPixels;
			
			_w = w;
		}
		return _w;
	}
	
	public static double getH()
	{
		int w = App.get().getResources().getDisplayMetrics().widthPixels;
		
		if(w < App.get().getResources().getDisplayMetrics().heightPixels)
			w = App.get().getResources().getDisplayMetrics().heightPixels;
		
		return w;
	}
	
    public static int getDisplayW()
    {
        return getResources().getDisplayMetrics().widthPixels;
    }

    public static int getScreenSize()
    {
        return getResources().getConfiguration().screenLayout &
               Configuration.SCREENLAYOUT_SIZE_MASK;
    }

    public static int getDisplayH()
    {
        return getResources().getDisplayMetrics().heightPixels;
    }


    public static int getDisplayMin()
    {
        return Math.min(getDisplayW(), getDisplayH());
    }

    public static int getDisplayMax()
    {
        return Math.max(getDisplayW(), getDisplayH());
    }

    public static float getRealSize(int num)
    {
        return num * getDensity();
    }

    public static int getRealSizeInt(int num)
    {
        return (int)(num * getDensity());
    }

    public static float getRealSize(float num)
    {
        return num * getDensity();
    }

    public static float getRealTextSize(float num)
    {
        return num * getDensity() / getScale();
    }
    
    public static float getTextSize(float num)
    {
        return num / getScale();
    }


    public static boolean getBool(int id)
    {
        return getResources().getBoolean(id);
    }

    public static DisplayMetrics getDisplayMetrics()
    {
        return getResources().getDisplayMetrics();
    }

    private static Boolean isFablet;
    @SuppressWarnings("SuspiciousNameCombination")
    public static boolean isFablet()
    {
        if(isFablet==null)
        {
            DisplayMetrics dm = getDisplayMetrics();

            double d = Math.pow(Math.pow(dm.widthPixels,2)+Math.pow(dm.heightPixels,2), 0.5)/ dm.densityDpi;

            isFablet = d >= 5 && d < 7;
        }

        return isFablet;

    }


    static Boolean is_phone;
    @SuppressWarnings("SuspiciousNameCombination")
    public static boolean IsPhone()
    {

        if(is_phone==null)
        {
            DisplayMetrics dm = getDisplayMetrics();

            double d = Math.pow(Math.pow(dm.widthPixels,2)+Math.pow(dm.heightPixels,2), 0.5) / dm.densityDpi;

            is_phone = d < 5;
        }

        return is_phone;
    }

}
