package com.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import com.utils.VovaActivity.IActivityHandler;
import com.utils.VovaActivity.IStateSaver;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RelativeLayout;

public class EventsUtils
{

	public static final String EVENTS = "events";
	static private HashMap<Integer, String> map_names = new HashMap<Integer, String>();
	static private WeakHandler handler;
	static private Boolean loop = false;
	static ArrayList<IBinder> list_binders = new ArrayList<IBinder>();
	static HashMap<Integer, ArrayList<IBinder>> map_directeds = new HashMap<Integer, ArrayList<IBinder>>();

	
	public static void Event(final Integer event)
	{
		Event(event, null, null);
	}

	public static void Event(final Integer event, Object param)
	{
		Event(event, null, param);
	}

	public static void Event(final Integer event, final String comment,
			final Object param)
	{

		if(handler == null)
		{
			handler = new WeakHandler(Looper.getMainLooper());
		}

		if(loop)
		{
			handler.post(new Runnable()
			{

				@Override
				public void run()
				{
					Event(event, comment, param);
				}
			});

			return;
		}

		synchronized (loop)
		{
			if(loop)
			{
				handler.postDelayed(new Runnable()
				{

					@Override
					public void run()
					{
						Event(event, comment, param);
					}
				}, 100L);

				return;
			}

			loop = true;
		}

		if(map_directeds.containsKey(event))
		{
			synchronized (map_directeds)
			{
				ArrayList<IBinder> list = map_directeds.get(event);

				for (IBinder b : list)
				{
					b.handle(event, param);
				}
			}

		} else
		{
			int n = list_binders.size();

			ArrayList<IBinder> cashe_list_binders = new ArrayList<IBinder>();

			synchronized (list_binders)
			{
				for (int i = 0; i < n; i++)
				{
					cashe_list_binders.add(list_binders.get(i));
				}
			}

			for (IBinder b : cashe_list_binders)
			{
				try
				{
					if(list_binders.contains(b))
					{
						b.handle(event, param);
					}
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			
			Log.w(EVENTS, GetName(event)+" "+(param==null?"":param));

		}
		
		

		loop = false;

	}

	

	static public HashMap<Integer, String> Names()
	{
		return map_names;
	}
	
	static public String GetName(int id)
	{
		if(map_names.containsKey(id)) {
			return map_names.get(id);
		}
		
		return String.valueOf(false);
	}


	@SuppressWarnings("rawtypes")
	static public void AddNames(Class cl)
	{

		HashMap<Integer, String> map = new HashMap<Integer, String>();

		Field[] declaredFields = cl.getDeclaredFields();
	
		for (Field field : declaredFields)
		{
			if(java.lang.reflect.Modifier.isStatic(field.getModifiers()))
			{

				if(field.getType() == Integer.class)
				{
					try
					{
						map.put((Integer) field.get(null), field.getName());
					} catch (Exception e)
					{
						// e.printStackTrace();
					}
				}

			}
		}

		map_names.putAll(map);

	}

	static public void AddName(Integer id, String name)
	{
		map_names.put(id, name);
	}
	
	static public int Id(String name)
	{
		int id = name.hashCode();
		map_names.put(id, name);
		
		return id;
	}
	
	
	public static void BindDirected(IBinder binder, Integer event)
	{
		ArrayList<IBinder> list = map_directeds.get(event);

		if(list == null)
		{
			list = new ArrayList<IBinder>();
			map_directeds.put(event, list);
		}

		if(!list.contains(binder)) {
			list.add(binder);
		}

	}

	public static void UnbindDirected(IBinder binder, Integer event)
	{
		ArrayList<IBinder> list = map_directeds.get(event);

		if(list == null)
		{
			list = new ArrayList<IBinder>();
			map_directeds.put(event, list);
		}

		list.remove(binder);

	}

	public static boolean Bind(IBinder binder)
	{
		if(!list_binders.contains(binder))
		{
			list_binders.add(0, binder);

			return true;
		}

		return false;
	}

	public static void Unbind(IBinder binder)
	{
		list_binders.remove(binder);
	}

	public static interface IBinder
	{
		void handle(Integer event, Object param);

	}

	
	
	public static abstract class BindedRelativeLayout extends RelativeLayout
			implements IBinder
	{

		public BindedRelativeLayout(Context context)
		{
			super(context);
		}

		public BindedRelativeLayout(Context context, AttributeSet attrs)
		{
			super(context, attrs);
		}

		@Override
		protected void onAttachedToWindow()
		{
			Bind(this);
			super.onAttachedToWindow();
		}

		@Override
		protected void onDetachedFromWindow()
		{
			Unbind(this);
			super.onDetachedFromWindow();
		}

	}

	public static abstract class GODRelativeLayout
			extends BindedRelativeLayoutWithActivityHandler implements IStateSaver
	{

		public GODRelativeLayout(Context context)
		{
			super(context);
			
			VovaActivity.AddStateSaverAndRestore(this);
		}

		@Override
		public String getSaveStateId()
		{
			return getClass().toString();
		}

	}

	public static abstract class BindedRelativeLayoutWithActivityHandler
			extends RelativeLayout implements IBinder, IActivityHandler
	{

		public BindedRelativeLayoutWithActivityHandler(Context context)
		{
			super(context);
		}

		abstract public void onBind();

		@Override
		protected void onAttachedToWindow()
		{
			if(Bind(this))
			{
				onBind();
			}

			VovaActivity.AddHandler(getContext(), this);
			super.onAttachedToWindow();
		}

		@Override
		protected void onDetachedFromWindow()
		{
			Unbind(this);
			VovaActivity.RemoveHandler(getContext(), this);
			super.onDetachedFromWindow();
		}

		@Override
		public void onDestroy()
		{

		}

		@Override
		public void onStart()
		{

		}

		@Override
		public void onStop()
		{

		}

		@Override
		public void onResume()
		{
			if(Bind(this))
			{
				onBind();
			}
		}

		@Override
		public void onPause()
		{
			Unbind(this);
		}

	}

	public static abstract class BindedActivity extends Activity
			implements IBinder
	{

		@Override
		protected void onCreate(Bundle savedInstanceState)
		{
			Bind(this);
			super.onCreate(savedInstanceState);
		}

		@Override
		protected void onDestroy()
		{
			Unbind(this);
			super.onDestroy();
		}

	}

	public static abstract class BindedFragmentActivity extends FragmentActivity
			implements IBinder
	{

		@Override
		protected void onSaveInstanceState(Bundle outState)
		{
			outState.putString("WORKAROUND_FOR_BUG_19917_KEY",
					"WORKAROUND_FOR_BUG_19917_VALUE");
			super.onSaveInstanceState(outState);
		}

		@Override
		protected void onCreate(Bundle savedInstanceState)
		{
			Bind(this);
			super.onCreate(savedInstanceState);
		}

		@Override
		protected void onDestroy()
		{
			Unbind(this);
			super.onDestroy();
		}

	}

}
