package com.utils;

import com.utils.shorts.Q;

import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.style.TextAppearanceSpan;


public class CharsStyler {
	int start = 0;
	int end = 0;
	SpannableStringBuilder builder = new SpannableStringBuilder();
	
	public CharsStyler appendWithStyle(CharSequence str, int appearance) {
		TextAppearanceSpan span = new TextAppearanceSpan(App.get(), appearance);
		
		builder.append(str);
		end = builder.length();
		builder.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		start = builder.length();
		
		return this;
	}
	
	public CharsStyler appendWithSize(CharSequence str, int size) {
		
		size = (int) Q.getRealSize(size);
		
		TextAppearanceSpan span = new TextAppearanceSpan(null, 0, size, null,null);
	
		builder.append(str);
		end = builder.length();
		builder.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		start = builder.length();

		return this;
	}

	public CharsStyler append(CharSequence str, int size, int color) {
		return append(str, size, color, Typeface.NORMAL);
	}


	public CharsStyler append(CharSequence str, int size, int color, int style) {
		
		size = (int) Q.getRealSize(size);

		TextAppearanceSpan span = new TextAppearanceSpan(null, style, size, new ColorStateList(
				new int[][]{{0}}, new int[]{ color}
		),null);

		builder.append(str);
		end = builder.length();
		builder.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		start = builder.length();

		return this;
	}

	public CharsStyler append(CharSequence str, int color) {

		TextAppearanceSpan span = new TextAppearanceSpan(null, 0, 0, new ColorStateList(
				new int[][]{{0}}, new int[]{ color}
		),null);

		builder.append(str);
		end = builder.length();
		builder.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		start = builder.length();

		return this;
	}
	
	public CharsStyler append(String str) {
		builder.append(str);
		start = builder.length();
		
		return this;
	}
	
	public static CharsStyler createWithStyle(CharSequence str, int appearance) {
		return new CharsStyler().appendWithStyle(str, appearance);
	}
	
	public static CharsStyler createWithSize(CharSequence str, int size) {
		return new CharsStyler().appendWithSize(str, size);
	}

	public static CharsStyler create(CharSequence str, int size, int color, int style) {
		return new CharsStyler().append(str, size, color, style);
	}

	public static CharsStyler create(CharSequence str, int size, int color) {
		return new CharsStyler().append(str, size, color);
	}

	public static CharsStyler create(CharSequence str, int color) {
		return new CharsStyler().append(str, color);
	}

	public static CharsStyler create() {
		return new CharsStyler();
	}

	public Spanned Get() {
		
		return new SpannedString(builder);
	}
	
}
