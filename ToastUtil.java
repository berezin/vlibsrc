package com.utils;

import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

/**
 * Created by berezin-ds on 02.06.2015.
 */
public class ToastUtil {

    static public void show(final String text)
    {
        if(Looper.getMainLooper()!=Looper.myLooper())
        {
            new Handler(Looper.getMainLooper()).post(new Runnable()
            {

                @Override
                public void run()
                {
                    show(text);
                }
            });
        }
        else
        {
            if(text!=null)
            {
                Toast.makeText(App.get(), text, Toast.LENGTH_SHORT).show();
            }
        }

    }
}