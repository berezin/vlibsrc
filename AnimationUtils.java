package com.utils;

import android.os.Build;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.utils.shorts.A;

/**
 * Created by berezin-ds on 24.06.2015.
 */
public class AnimationUtils {
	
    public static interface IEvent {
        void onEnd();
    }

    public static class DataPressScaling
    {

        boolean is_pressed = false;

        int dur;
        float f;
        public DataPressScaling(int dur, float scale) {
            this.dur = dur;
            this.f = scale;

        }


        public void drawableStateChanged(View v, boolean isPressed)
        {

            if(Build.VERSION.SDK_INT<=10) {
                return;
            }

            if(isPressed) {
                if(!is_pressed) {
                    A.scale_for_setPressScalable(v, dur, 1, f);
                }
                is_pressed = true;
            } else {
                if(is_pressed) {
                    A.scale_for_setPressScalable(v, dur, f, 1f);
                }
                is_pressed = false;
            }

        }

    }

    public static void scale_for_setPressScalable(final View v, final int dur,
                                                  final float from, final float to)
    {


        v.clearAnimation();

        AnimationSet anims = new AnimationSet(true);
        anims.setFillAfter(true);
        anims.setDuration(dur);

        ScaleAnimation animation = new ScaleAnimation(from, to, from, to,
                                                      RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                                                      RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(dur);
        animation.setFillAfter(true);
        animation.setInterpolator(new DecelerateInterpolator());

        anims.addAnimation(animation);

        AlphaAnimation animation2 = new AlphaAnimation(from, to);
        animation2.setDuration(dur);

        animation2.setFillAfter(true);
        anims.addAnimation(animation2);




        v.startAnimation(anims);

    }

    public static void changeText(final TextView text,final CharSequence str)
    {
        changeText(text, str, 300, Dimen.d5, null, null);
    }
    public static void changeText(final TextView text,final CharSequence str,final int dur,final int r)
    {
        changeText(text, str, dur, r, null, null);
    }
    public static void changeText(final TextView text,final CharSequence str,final int dur,final int r,final IEvent event_change)
    {
        changeText(text, str, dur, r, event_change, null);
    }
    public static void changeText(final TextView text,final CharSequence str,final int dur,final int r, final IEvent event_change, final IEvent event_post)
    {
        String str_old = text.getText().toString();

        if(!str_old.equals(str))
        {


            A.to_left_and_hide(text, dur, r, true, new IEvent() {

                @Override
                public void onEnd() {

                    text.setVisibility(View.INVISIBLE);


                    text.post(new Runnable() {

                        @Override
                        public void run() {

                            text.setText(str);

                            if (event_change != null) {
                                event_change.onEnd();
                            }

                            A.to_left_and_hide(text, dur, -r, false, event_post);
                        }
                    });


                }
            });

        }
    }

    public static void to_left_and_hide(final View v, int dur, int r,
                                       final boolean hide)
    {
        to_left_and_hide(v, dur, r, hide, null);
    }

    public static void to_left_and_hide(final View v, int dur, int r,
                                       final boolean hide, final IEvent event)
    {

        AnimationSet anims = new AnimationSet(true);
        if(!hide)
            anims.setInterpolator(new DecelerateInterpolator());
        else
            anims.setInterpolator(new AccelerateInterpolator());

        TranslateAnimation animation = new TranslateAnimation(hide ? 0 : r,
                                                              hide ? r : 0, 0, 0);
        animation.setDuration(dur);

        v.setVisibility(View.VISIBLE);

        animation.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(hide) {
                    v.setVisibility(View.GONE);
                } else {
                    v.setVisibility(View.VISIBLE);
                }

                if(event != null)
                    event.onEnd();
            }
        });

        anims.addAnimation(animation);

        AlphaAnimation animation2 = new AlphaAnimation(hide ? 1 : 0, hide ? 0
                : 1);
        animation2.setDuration(dur);

        anims.addAnimation(animation2);

        v.startAnimation(anims);
    }

    public static void fromy_and_show(final View v, int dur, int r,
			final boolean hide)
	{
		fromy_and_show(v, dur, r, hide, null);
	}

	public static void fromy_and_show(final View v, int dur, int r,
			final boolean hide, final IEvent event)
	{

		AnimationSet anims = new AnimationSet(true);

		AccelerateDecelerateInterpolator in = new AccelerateDecelerateInterpolator();
		
		anims.setInterpolator(in);
		
		TranslateAnimation animation = new TranslateAnimation(0, 0, hide ? 0
				: r, hide ? r : 0);
		animation.setDuration(dur);

		v.setVisibility(View.VISIBLE);
		
		final DataAnimationOnScreen data = new DataAnimationOnScreen();

		animation.setAnimationListener(new AnimationListener()
		{

			@Override
			public void onAnimationStart(Animation animation)
			{
				data.valid(v, animation, this);
			}

			@Override
			public void onAnimationRepeat(Animation animation)
			{

			}

			@Override
			public void onAnimationEnd(Animation animation)
			{
				if(hide)
				{
					v.setVisibility(View.GONE);
				} else
				{
					v.setVisibility(View.VISIBLE);
				}

				if(event != null)
					event.onEnd();
			}
		});

		anims.addAnimation(animation);

		AlphaAnimation animation2 = new AlphaAnimation(hide ? 1 : 0, hide ? 0
				: 1);
		animation2.setDuration(dur);

		anims.addAnimation(animation2);

		v.startAnimation(anims);
	}
    
    public static void toup_alpha(View v, int dur, int r, float from_alpha, float to_alpha, final IEvent event)
	{
		AnimationSet anims = new AnimationSet(true);
		anims.setFillAfter(true);
		
		
		AlphaAnimation animation2 = new AlphaAnimation(from_alpha, to_alpha);
		animation2.setDuration(dur);
		animation2.setFillAfter(true);
		anims.addAnimation(animation2);
		
		TranslateAnimation animation = new TranslateAnimation(0, 0, 0, -r);
		animation.setDuration(dur);
		animation.setFillAfter(true);
		anims.addAnimation(animation);
		
		anims.setAnimationListener(new AnimationListener()
		{

			@Override
			public void onAnimationStart(Animation animation)
			{
				
			}

			@Override
			public void onAnimationRepeat(Animation animation)
			{
				
			}

			@Override
			public void onAnimationEnd(Animation animation)
			{
				if(event != null)
					event.onEnd();
			}
		});

		v.startAnimation(anims);
	}
    
    public static void to_up_and_hide(final View v, int dur, int r,
                                        final boolean hide)
    {
        to_up_and_hide(v, dur, r, hide, null);
    }

    public static void to_up_and_hide(final View v, int dur, int r,
                                        final boolean hide, final IEvent event)
    {

        AnimationSet anims = new AnimationSet(true);
        if(!hide)
            anims.setInterpolator(new DecelerateInterpolator());
        else
            anims.setInterpolator(new AccelerateInterpolator());

        TranslateAnimation animation = new TranslateAnimation(0, 0, hide ? 0 : -r,
                                                              hide ? -r : 0);
        animation.setDuration(dur);

        v.setVisibility(View.VISIBLE);

        animation.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(hide) {
                    v.setVisibility(View.GONE);
                } else {
                    v.setVisibility(View.VISIBLE);
                }

                if(event != null)
                    event.onEnd();
            }
        });

        anims.addAnimation(animation);

        AlphaAnimation animation2 = new AlphaAnimation(hide ? 1 : 0, hide ? 0
                : 1);
        animation2.setDuration(dur);

        anims.addAnimation(animation2);

        v.startAnimation(anims);
    }

    public static void toup(View v, int dur, int r) {
        toup(v, dur, r, null);
    }

    public static void toup(View v, int dur, int r, final IEvent event) {
        TranslateAnimation animation = new TranslateAnimation(0, 0, 0, -r);
        animation.setDuration(dur);
        animation.setFillAfter(true);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());

        animation.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (event != null) {
                    event.onEnd();
                }
            }
        });

        v.startAnimation(animation);
    }

    public static void fadeout(final View v, int dur)
    {
        fadeout(v, dur, null);
    }

    public static void fadeout(final View v, int dur, final IEvent event)
    {
        AlphaAnimation animation = new AlphaAnimation(1, 0);
        animation.setDuration(dur);
        v.setVisibility(View.VISIBLE);

        animation.setAnimationListener(new AnimationListener()
        {

            @Override
            public void onAnimationStart(Animation arg0)
            {

            }

            @Override
            public void onAnimationRepeat(Animation arg0)
            {

            }

            @Override
            public void onAnimationEnd(Animation arg0)
            {

                v.setVisibility(View.GONE);

                if(event != null) {
                    event.onEnd();
                }
            }
        });

        v.startAnimation(animation);

    }

    public static void fadein(final View v, int dur)
    {
        fadein(v, dur, null);
    }

    public static void fadein(final View v, int dur, final IEvent event)
    {
        AlphaAnimation animation = new AlphaAnimation(0, 1);
        animation.setDuration(dur);
        v.setVisibility(View.VISIBLE);

        animation.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation arg0) {

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {

            }

            @Override
            public void onAnimationEnd(Animation arg0) {


                if (event != null) {
                    event.onEnd();
                }
            }
        });

        v.startAnimation(animation);

    }
    
    public static void fadein(final View v, int dur, float from,final IEvent event)
	{
		AlphaAnimation animation = new AlphaAnimation(from, 1);
		animation.setDuration(dur);
		v.setVisibility(View.VISIBLE);
		
		final DataAnimationOnScreen data = new DataAnimationOnScreen();

		animation.setAnimationListener(new AnimationListener()
		{

			@Override
			public void onAnimationStart(Animation a)
			{	
				data.valid(v, a, this);
			}

			@Override
			public void onAnimationRepeat(Animation arg0)
			{
				
			}

			@Override
			public void onAnimationEnd(Animation arg0)
			{

				if(event != null)
					event.onEnd();
			}
		});

		v.startAnimation(animation);
	}
    
    
    public static void scale(final View v, int dur, float from)
	{
		scale(v, dur, from, 1f, null);
	}

	public static void scale(final View v, int dur, float from, float to)
	{
		scale(v, dur, from, to, null);
	}

	public static void scale(final View v, int dur, float from, float to,
			final IEvent event)
	{

		ScaleAnimation animation = new ScaleAnimation(from, to, from, to,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		animation.setDuration(dur);
		v.setVisibility(View.VISIBLE);

		animation.setAnimationListener(new AnimationListener()
		{

			@Override
			public void onAnimationStart(Animation arg0)
			{
				
			}

			@Override
			public void onAnimationRepeat(Animation arg0)
			{
				
			}

			@Override
			public void onAnimationEnd(Animation arg0)
			{

				if(event != null)
					event.onEnd();
			}
		});

		v.startAnimation(animation);
	}
    
    public static void scale_and_hide(final View v, int dur, float from, float to, boolean hide,
			final IEvent event)
	{

		ScaleAnimation animation = new ScaleAnimation(from, to, from, to,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		animation.setDuration(dur);
		
		AlphaAnimation alpha = new AlphaAnimation(hide?1f:0f, hide?0f:1f);
		
		AnimationSet set = new AnimationSet(true);
		set.addAnimation(animation);
		set.addAnimation(alpha);
		set.setDuration(dur);
		
		v.setVisibility(View.VISIBLE);

		animation.setAnimationListener(new AnimationListener()
		{

			@Override
			public void onAnimationStart(Animation arg0)
			{
				
			}

			@Override
			public void onAnimationRepeat(Animation arg0)
			{
				
			}

			@Override
			public void onAnimationEnd(Animation arg0)
			{

				if(event != null)
					event.onEnd();
			}
		});

		v.startAnimation(set);
	}
    
    
    
    public static void blimp(final View v, int dur, final float alpha, final IEvent event)
	{
		final int d = dur / 2;
		
		AlphaAnimation animation = new AlphaAnimation(1f, alpha);
		animation.setDuration(d);
		animation.setInterpolator(new DecelerateInterpolator());
		
		
		animation.setAnimationListener(new AEndListener(v)
		{
			
			@Override
			public void onAnimationEnd(Animation animation)
			{
				AlphaAnimation animation2 = new AlphaAnimation(alpha, 1f);
				animation2.setDuration(d);
				animation2.setInterpolator(new AccelerateInterpolator());
				animation2.setAnimationListener(new AListener(v, event));
				
				v.startAnimation(animation2);
			}
		});
		 


		v.startAnimation(animation);
	}
    
    public static void jump(final View v, int dur, final int r)
	{
		final int d = dur / 2;

		TranslateAnimation animation = new TranslateAnimation(0, 0, 0, -r);
		animation.setDuration(d);
		animation.setAnimationListener(new AnimationListener()
		{

			@Override
			public void onAnimationStart(Animation animation)
			{

			}

			@Override
			public void onAnimationRepeat(Animation animation)
			{

			}

			@Override
			public void onAnimationEnd(Animation animation)
			{
				TranslateAnimation animation2 = new TranslateAnimation(0, 0,
						-r, 0);
				animation2.setDuration(d);

				v.startAnimation(animation2);
			}
		});

		v.startAnimation(animation);
	}
    
    
    public abstract static class AEndListener implements AnimationListener {

    	DataAnimationOnScreen data;
    	View v;
    	
    	public AEndListener(View v) {

    		this.v = v;
    		data = new DataAnimationOnScreen();
    	}
    	
		@Override
		public void onAnimationStart(Animation animation)
		{
			data.valid(v, animation, this);
		}


		@Override
		public void onAnimationRepeat(Animation animation)
		{
			
		}
    	
    }
    
    public static class AListener implements AnimationListener {

    	
    	IEvent event;
    	DataAnimationOnScreen data;
    	View v;
    	
    	public AListener(View v, IEvent event) {
    		this.event = event;
    		this.v = v;
    		data = new DataAnimationOnScreen();
    	}
    	
		@Override
		public void onAnimationStart(Animation animation)
		{
			data.valid(v, animation, this);
		}

		@Override
		public void onAnimationEnd(Animation animation)
		{
			
		}

		@Override
		public void onAnimationRepeat(Animation animation)
		{
			if(event!=null) {
				event.onEnd();
			}
		}
    	
    }
    
    public static class DataAnimationOnScreen
	{
		public long start;
		
		public DataAnimationOnScreen()
		{
			start = System.currentTimeMillis();
		}
		
		
		
		
		public void valid(View view, Animation animation, AnimationListener listener)
		{
			if((System.currentTimeMillis() - start)>200)
			{
				
				animation.setAnimationListener(new AnimationListener()
				{
					
					@Override
					public void onAnimationStart(Animation animation)
					{
						
					}
					
					@Override
					public void onAnimationRepeat(Animation animation)
					{
						
					}
					
					@Override
					public void onAnimationEnd(Animation animation)
					{
						
					}
				});
				
				view.clearAnimation();
				
				if(listener!=null)
				{
					listener.onAnimationEnd(animation);
				}
			}
			
		}
		
	}
    
    static public class ResizeAnimation extends Animation
	{
		View view;
		int startH;
		int endH;
		int diff;

		public ResizeAnimation(View v, int newh)
		{
			view = v;
			startH = v.getLayoutParams().height;
			endH = newh;
			diff = endH - startH;
		}

		@Override
		protected void applyTransformation(float interpolatedTime,
				Transformation t)
		{
			view.getLayoutParams().height = startH
					+ (int) (diff * interpolatedTime);
			view.requestLayout();
		}

		@Override
		public void initialize(int width, int height, int parentWidth,
				int parentHeight)
		{
			super.initialize(width, height, parentWidth, parentHeight);
		}

		@Override
		public boolean willChangeBounds()
		{
			return true;
		}
	}

}
