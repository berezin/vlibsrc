package com.utils;


import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("ALL")
public class ThreadTransanction {
	private ThreadTransanction()
	{
		
	}
	//private static HashMap<String,Future> mMapFutures = new HashMap<String, Future>();
	
	private static ThreadPoolExecutor mThreadPool;
	private static ThreadPoolExecutor mThreadPool_http;
	private static ThreadPoolExecutor mThreadPool_http_main;
	private static ThreadPoolExecutor mThreadPool_ui;
	
	static Thread ThreadMain;
	private static ThreadPoolExecutor threadPool() {
		if(mThreadPool == null) {
			ThreadMain = Thread.currentThread();
			mThreadPool = new ThreadPoolExecutor(
					1,
					1,
					10,
					TimeUnit.SECONDS,
					new LinkedBlockingQueue<Runnable>(),new ThreadFactory(){

						public Thread newThread(Runnable r) {
							Thread t = new Thread(r);
							t.setPriority(Thread.MIN_PRIORITY);
							//t.setDaemon(isDaemon)
							return t;
						}
						
					}
				            );
			mThreadPool.prestartAllCoreThreads();
		}
		return mThreadPool;
	}
	public static ThreadPoolExecutor threadPool_http() {
		
		if(mThreadPool_http == null) {
			mThreadPool_http = new ThreadPoolExecutor(
					4,
					4,
					1,
					TimeUnit.SECONDS,
					new LinkedBlockingQueue<Runnable>(),new ThreadFactory(){

						public Thread newThread(Runnable r) {
							Thread t = new Thread(r);
							t.setPriority(Thread.MIN_PRIORITY);
							
							return t;
						}
						
					}
				            );
			mThreadPool_http.prestartAllCoreThreads();
		}
		return mThreadPool_http;
	}
	private static ThreadPoolExecutor threadPool_http_main() {
		if(mThreadPool_http_main == null) {
			mThreadPool_http_main = new ThreadPoolExecutor(
					1,
					1,
					10,
					TimeUnit.SECONDS,
					new LinkedBlockingQueue<Runnable>(),new ThreadFactory(){

						public Thread newThread(Runnable r) {
							Thread t = new Thread(r);
							t.setPriority(Thread.MIN_PRIORITY);
							//t.setDaemon(isDaemon)
							return t;
						}
						
					}
				            );
			mThreadPool_http_main.prestartAllCoreThreads();
		}
		return mThreadPool_http_main;
	}
	
	private static ThreadPoolExecutor threadPool_ui() {
		if(mThreadPool_ui == null) {
			
			mThreadPool_ui = new ThreadPoolExecutor(
					1,
					1,
					10,
					TimeUnit.SECONDS,
					new LinkedBlockingQueue<Runnable>(),new ThreadFactory(){

						public Thread newThread(Runnable r) {
							Thread t = new Thread(r);
							t.setPriority(Thread.MAX_PRIORITY);
							//t.setDaemon(isDaemon)
							return t;
						}
						
					}
				            );
			mThreadPool_ui.prestartAllCoreThreads();
		}
		return mThreadPool_ui;
	}

	public enum ThreadStatus
	{
		OK,
		CANCEL
	}
	public interface ThreadRunnable<T>
	{
		void result(ThreadStatus aStatus,T aResult);
		T run();
	}
	@SuppressWarnings("unused")
	private static class ThreadRunnableClass<T>
	{
		public ThreadRunnable<T> threadRunnable;
		public T result;
		public ThreadStatus status;
		public ThreadRunnableClass(ThreadRunnable<T> aThreadRunnable)
		{
			threadRunnable = aThreadRunnable;
		}
	}

	
	public static class TT extends ThreadTransanction
	{
		
	}
	

	static class TransanctionRunnable<T> implements Runnable
	{

		String mName;
		ThreadPoolExecutor mExecutor;
		ThreadRunnable<T> mRunnable;
		
		public TransanctionRunnable(String aName, ThreadPoolExecutor aExecutor, ThreadRunnable<T> aRunnable)
		{
			mName = aName;
			mExecutor = aExecutor;
			mRunnable = aRunnable;
		}
		
		
		@Override
		public void run()
		{
			long start = System.currentTimeMillis();
			
			BAssert.log("Thread start: "+mName);
			
			try
			{
				final T o = mRunnable.run();
				
				
				handler.post(new Runnable() {
					
					public void run() {
						
						try
						{
							mRunnable.result(ThreadStatus.OK, o);
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
						
					}
				});
				
				//if(Vova.IsLoging())
				{
					StringBuilder sb = new StringBuilder();
					
					sb.append("_Thread stop (");
					
					sb.append(((double)(System.currentTimeMillis() - start)/1000));
					
					sb.append("sec): ");
					
					sb.append(mName);

					BAssert.log(sb.toString());
				}
				
				
				
			
				
			
			}
			catch(Exception ex)
			{
				handler.post(new Runnable() {
					
					public void run() {
						
						try
						{
							mRunnable.result(ThreadStatus.CANCEL, null);
						} catch (Exception e)
						{
							e.printStackTrace();
						}
						
					}
				});
				
				//if(Vova.IsLoging())
					BAssert.log("Thread stop with status 'Cancel': "+mName+" "+ex);
				
				ex.printStackTrace();
			}
		}
		
	}
	
	static Handler handler;
	
	public static class Result<T>
	{
		public T property;
	}
	
	public static <T> void execute(String aName,ThreadPoolExecutor aExecutor, final ThreadRunnable<T> aRunnable)
	{
		
		
		
		
		
		/*
		new AsyncTask<Object, Object, T>()
				{

					@Override
					protected void onPostExecute(T result) {
						super.onPostExecute(result);
						

						try
						{
							aRunnable.result(ThreadStatus.OK, result);
						} catch (Exception e)
						{
							e.printStackTrace();
						}

					}

					@Override
					protected T doInBackground(Object... params) {
						

						try
						{
							return aRunnable.run();
						} catch (Exception e)
						{
							e.printStackTrace();
						}
						
						
						return null;
						
					}
			
				}.execute();
			*/	
		
		/*
		final Result<T> result = new Result<>();
		
		execute(new Runnable()
		{
			
			@Override
			public void run()
			{
				
				try
				{
					result.property = aRunnable.run();
				}
				catch (Exception e)
				{
					
					e.printStackTrace();
				}
				
			}
		}, new Runnable()
		{
			
			@Override
			public void run()
			{
				try
				{
					aRunnable.result(ThreadStatus.OK, result.property);
				}
				catch (Exception e)
				{
					
					e.printStackTrace();
				}
			}
		});
		*/
		
		if(handler==null)
		{
			handler = new Handler(Looper.getMainLooper());
		}
		
		aExecutor.execute(new TransanctionRunnable<T>(aName, aExecutor, aRunnable));
		
	}
	
	public static <T> void execute( String aName,ThreadRunnable<T> aRunnable)
	{
		execute("* "+aName, threadPool(), aRunnable);	
	}
	public static <T> void execute_http( String aName,ThreadRunnable<T> aRunnable)
	{
		execute("+ "+aName, threadPool_http(), aRunnable);	
	}
	public static <T> void execute_ui( String aName,ThreadRunnable<T> aRunnable)
	{
		execute("ui "+aName, threadPool_ui(), aRunnable);	
	}
	public static <T> void execute_http_main( String aName,ThreadRunnable<T> aRunnable)
	{
		execute("= "+aName, threadPool_http_main(), aRunnable);	
	}
	
	public static class BAssert
	{
		static public void log(String text)
		{
			Log.v("log",text);
		}
	}
}

