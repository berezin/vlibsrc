/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.utils.image;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import com.utils.ThreadTransanction;
import com.utils.Utils;
import com.utils.image.ImageCache.ImageCacheParams;
import com.utils.image.Malevich.ImageDecodedListener;
import com.utils.shorts.A;
import com.utils.ui.IImage;
import com.utils.ui.ImageLayout;

/**
 * This class wraps up completing some arbitrary long running work when loading
 * a bitmap to an ImageView. It handles things like using a memory and disk
 * cache, running the work in a background thread and setting a placeholder
 * image.
 */
public class ImageWorker
{
	private static final String TAG = "Malevich: ImageWorker";
	private ImageCache mImageCache;
	private ImageCache.ImageCacheParams mImageCacheParams;
	private Bitmap mLoadingBitmap;
	private boolean mExitTasksEarly = false;
	protected boolean mPauseWork = false;
	private final Object mPauseWorkLock = new Object();

	protected Resources mResources;

	private final boolean debug;

	//public static int HTTP_CACHE_SIZE = 10 * 1024 * 1024; // 10MB
	// private static final String HTTP_CACHE_DIR = "http";
	private static final int IO_BUFFER_SIZE = 8 * 1024;

	protected ImageWorker(Context context, boolean debug,
			ImageCacheParams params)
	{
		this.debug = debug;

		mResources = context.getResources();
	}

	/**
	 * Load an image specified by the data parameter into an ImageView A memory
	 * and disk cache will be used if an {@link ImageCache} has been added using
	 * {@link ImageWorker#addImageCache( ImageCache.ImageCacheParams)}. If the
	 * image is found in the memory cache, it is set immediately, otherwise an
	 * {@link AsyncTask} will be created to asynchronously load the bitmap.
	 *
	 * @param data
	 *            The URL of the image to download.
	 * @param imageView
	 *            The ImageView to bind the downloaded image to.
	 */

	public void loadVovaImage(Object data, IImage imageView, int reqWidth,
			int reqHeight, Malevich.ImageDecodedListener imageDecodedListener)
	{
		if(data == null)
		{
			return;
		}

		if(debug)
		{
			Log.w(TAG, "start " + data);
		}

		if(imageView != null)
		{
			imageView.Clear();
		}

		BitmapDrawable value = null;

		// If bitmap setted, don't use cache
		if(data instanceof Bitmap)
		{
			value = new BitmapDrawable(mResources, (Bitmap) data);
		}

		// Check cache
		if(value == null && mImageCache != null)
		{
			value = mImageCache.getBitmapFromMemCache(String.valueOf(data));
		}

		if(value == null && data instanceof BitmapDrawable)
		{
			value = (BitmapDrawable) data;
		}

		if(value == null && data instanceof Integer)
		{
		
			value = new BitmapDrawable(mResources,
					Malevich.Utils.decodeSampledBitmapFromResource(mResources,
							(int) data, reqWidth, reqHeight, null, debug));
		}

		if(value != null)
		{
			// Bitmap found in memory cache
			if(imageView != null)
			{
				imageView.Set(value.getBitmap(), true);
			}
		} else
		{
			if(imageView == null || cancelPotentialWork(data, imageView))
			{
				final BitmapWorkerTask task = new BitmapWorkerTask(data,
						imageView, reqWidth, reqHeight, imageDecodedListener);

				if(imageView != null)
				{
					final AsyncDrawable asyncDrawable = new AsyncDrawable(
							mResources, mLoadingBitmap, task);

					imageView.setTag(asyncDrawable);
				}

				if(imageView != null)
				{
					task.executeOnExecutor(
							ThreadTransanction.threadPool_http());
				} else
				{
					task.executeOnExecutor(AsyncTask.DUAL_THREAD_EXECUTOR);
				}
			}
		}
	}

	public void loadImage(Object data, ImageView imageView, int reqWidth,
			int reqHeight, Malevich.ImageDecodedListener imageDecodedListener)
	{
		if(data == null)
		{
			return;
		}

		if(imageView != null)
		{
			imageView.setImageBitmap(null);
		}

		BitmapDrawable value = null;

		// If bitmap setted, don't use cache
		if(data instanceof Bitmap)
		{
			value = new BitmapDrawable(mResources, (Bitmap) data);
		}

		// Check cache
		if(value == null && mImageCache != null)
		{
			value = mImageCache.getBitmapFromMemCache(String.valueOf(data));
		}

		if(value == null && data instanceof BitmapDrawable)
		{
			value = (BitmapDrawable) data;
		}

		if(value == null && data instanceof Integer)
		{
			value = new BitmapDrawable(mResources,
					Malevich.Utils.decodeSampledBitmapFromResource(mResources,
							(int) data, reqWidth, reqHeight, null, debug));
		}

		if(value != null)
		{
			// Bitmap found in memory cache
			if(imageView != null)
			{

				imageView.setImageDrawable(value);
			}
		} else
		{
			if(imageView == null || cancelPotentialWork(data, imageView))
			{
				final BitmapWorkerTask task = new BitmapWorkerTask(data,
						imageView, reqWidth, reqHeight, imageDecodedListener);

				if(imageView != null)
				{
					final AsyncDrawable asyncDrawable = new AsyncDrawable(
							mResources, mLoadingBitmap, task);

					imageView.setTag(asyncDrawable);
				}

				if(imageView != null)
				{
					task.executeOnExecutor(
							ThreadTransanction.threadPool_http());
				} else
				{
					task.executeOnExecutor(AsyncTask.DUAL_THREAD_EXECUTOR);
				}
			}
		}
	}

	/**
	 * Set placeholder bitmap that shows when the the background thread is
	 * running.
	 *
	 * @param bitmap
	 */
	public void setLoadingImage(Bitmap bitmap)
	{
		mLoadingBitmap = bitmap;
	}

	/**
	 * Set placeholder bitmap that shows when the the background thread is
	 * running.
	 *
	 * @param resId
	 */
	public void setLoadingImage(int resId)
	{
		mLoadingBitmap = BitmapFactory.decodeResource(mResources, resId);
	}

	/**
	 * Adds an {@link ImageCache} to this {@link ImageWorker} to handle disk and
	 * memory bitmap caching.
	 * 
	 * @param cacheParams
	 *            The cache parameters to use for the image cache.
	 */
	public void addImageCache(ImageCache.ImageCacheParams cacheParams)
	{
		mImageCacheParams = cacheParams;
		mImageCache = ImageCache.getInstance(mImageCacheParams, debug);
	}

	/**
	 * If set to true, the image will fade-in once it has been loaded by the
	 * background thread.
	 */
	public void setImageFadeIn(boolean fadeIn)
	{
	}

	public void setExitTasksEarly(boolean exitTasksEarly)
	{
		mExitTasksEarly = exitTasksEarly;
		setPauseWork(false);
	}


	/**
	 * @return The {@link ImageCache} object currently being used by this
	 *         ImageWorker.
	 */
	protected ImageCache getImageCache()
	{
		return mImageCache;
	}

	/**
	 * Cancels any pending work attached to the provided ImageView.
	 * 
	 * @param imageView
	 */
	public static void cancelWork(ImageLayout imageView)
	{
		final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(
				imageView);
		if(bitmapWorkerTask != null)
		{
			bitmapWorkerTask.cancel(true);
		}
	}

	/**
	 * Returns true if the current work has been canceled or if there was no
	 * work in progress on this image view. Returns false if the work in
	 * progress deals with the same data. The work is not stopped in that case.
	 */
	public static boolean cancelPotentialWork(Object data, IImage imageView)
	{
		// BEGIN_INCLUDE(cancel_potential_work)
		final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(
				imageView);

		if(bitmapWorkerTask != null)
		{
			final Object bitmapData = bitmapWorkerTask.mData;
			if(bitmapData == null || data == null || !bitmapData.equals(data))
			{
				bitmapWorkerTask.cancel(true);
			} else
			{
				// The same work is already in progress.
				return false;
			}
		}
		return true;
		// END_INCLUDE(cancel_potential_work)
	}

	private boolean cancelPotentialWork(Object data, ImageView imageView)
	{
		final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(
				imageView);

		if(bitmapWorkerTask != null)
		{
			final Object bitmapData = bitmapWorkerTask.mData;
			if(bitmapData == null || data == null || !bitmapData.equals(data))
			{
				bitmapWorkerTask.cancel(true);
			} else
			{
				// The same work is already in progress.
				return false;
			}
		}
		return true;
	}

	private static BitmapWorkerTask getBitmapWorkerTask(IImage imageView)
	{
		if(imageView != null && imageView.getTag() != null
				&& imageView.getTag() instanceof AsyncDrawable)
		{
			final AsyncDrawable asyncDrawable = (AsyncDrawable) imageView
					.getTag();
			return asyncDrawable.getBitmapWorkerTask();
		}
		return null;
	}

	/**
	 * @param imageView
	 *            Any imageView
	 * @return Retrieve the currently active work task (if any) associated with
	 *         this imageView. null if there is no such task.
	 */
	private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView)
	{
		if(imageView != null && imageView.getTag() != null
				&& imageView.getTag() instanceof AsyncDrawable)
		{
			final AsyncDrawable asyncDrawable = (AsyncDrawable) imageView
					.getTag();
			return asyncDrawable.getBitmapWorkerTask();
		}
		return null;
	}

	/**
	 * The actual AsyncTask that will asynchronously process the image.
	 */
	private class BitmapWorkerTask extends AsyncTask<Void, Void, BitmapDrawable>
	{
		private Object mData;
		private int reqWidth;
		private int reqHeight;
		private WeakReference<IImage> imageViewReference = null;
		private WeakReference<ImageView> imageReference = null;

		public BitmapWorkerTask(Object data, IImage imageView, int reqWidth,
				int reqHeight,
				Malevich.ImageDecodedListener imageDecodedListener)
		{
			mData = data;
			this.reqWidth = reqWidth;
			this.reqHeight = reqHeight;
			if(imageView != null)
			{
				imageViewReference = new WeakReference<IImage>(imageView);
			}
		}

		public BitmapWorkerTask(Object data, ImageView imageView, int reqWidth,
				int reqHeight, ImageDecodedListener imageDecodedListener)
		{
			mData = data;
			this.reqWidth = reqWidth;
			this.reqHeight = reqHeight;
			if(imageView != null)
			{
				imageReference = new WeakReference<ImageView>(imageView);
			}
		}

		/**
		 * Background processing.
		 */

		protected void doBackground(Void... params)
		{

			if(!isCancelled()
					&& !mExitTasksEarly)
			{
				
				ImageProcess.Get((String) mData, reqWidth, reqHeight, true);
			}

		}

		@Override
		protected BitmapDrawable doInBackground(Void... params)
		{
		
			if(debug)
			{
				Log.d(TAG, "doInBackground - starting work");
			}

			if(imageViewReference == null && imageReference == null) {

				doBackground(params);
				return null;
			}

			final String dataString = (String) mData;
			Bitmap bitmap = null;
			BitmapDrawable drawable = null;
	
		

			if(bitmap == null && !isCancelled() && !mExitTasksEarly)
			{
				bitmap = ImageProcess.Get(dataString, reqWidth, reqHeight, !hasAttachedImageView());
			}

			if(bitmap != null)
			{

				drawable = new BitmapDrawable(mResources, bitmap);

				if(imageViewReference != null
						&& imageViewReference.get() != null)
				{
					imageViewReference.get().onAsyncSet(bitmap);
				}

				if(mImageCache != null)
				{
					if(mImageCache.getMemoryCache() != null)
					{
						mImageCache.getMemoryCache().put(dataString, drawable);
					}
				}
				
				
			}

			if(debug)
			{
				Log.d(TAG, "doInBackground - finished work");
			}

			return drawable;
			// END_INCLUDE(load_bitmap_in_background)
		}

		/**
		 * Once the image is processed, associates it to the imageView
		 */
		@Override
		protected void onPostExecute(BitmapDrawable value)
		{
			// BEGIN_INCLUDE(complete_background_work)
			// if cancel was called on this task or the "exit early" flag is set
			// then we're done
			if(isCancelled() || mExitTasksEarly)
			{
				value = null;
			}

			if(imageViewReference == null && imageReference == null)
			{
				return;
			} else if(imageViewReference != null)
			{
				final IImage imageView = getAttachedImageView_();
				if(value != null && imageView != null)
				{
					if(debug)
					{
						Log.d(TAG, "onPostExecute - setting bitmap");
					}
					setImageDrawable(imageView, value);

					imageView.onAnimate();

				}
			} else
			{
				final ImageView imageView = getAttachedImage_();
				if(value != null && imageView != null)
				{
					if(debug)
					{
						Log.d(TAG, "onPostExecute - setting bitmap");
					}
					setImageDrawable(imageView, value);

					if(Utils.getOsVersion() > 15)
					{
						A.fadein(imageView, 300, 0.5f, null);
					}
				}
			}

			// END_INCLUDE(complete_background_work)
		}

		@Override
		protected void onCancelled(BitmapDrawable value)
		{
			super.onCancelled(value);
			synchronized (mPauseWorkLock)
			{
				mPauseWorkLock.notifyAll();
			}
		}

		/**
		 * Returns the ImageView associated with this task as long as the
		 * ImageView's task still points to this task as well. Returns null
		 * otherwise.
		 */

		private boolean hasAttachedImageView()
		{

			return getAttachedImage_() != null
					|| getAttachedImageView_() != null;
		}

		private IImage getAttachedImageView_()
		{

			if(imageViewReference != null)
			{
				final IImage imageView = imageViewReference.get();

				if(imageView != null)
				{
					final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(
							imageView);

					if(this == bitmapWorkerTask)
					{
						return imageView;
					}
				}

			}

			return null;
		}

		private ImageView getAttachedImage_()
		{

			if(imageReference != null)
			{
				final ImageView imageView = imageReference.get();
				if(imageView != null)
				{
					final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(
							imageView);

					if(this == bitmapWorkerTask)
					{
						return imageView;
					}
				}

			}

			return null;
		}
	}

	/**
	 * A custom Drawable that will be attached to the imageView while the work
	 * is in progress. Contains a reference to the actual worker task, so that
	 * it can be stopped if a new binding is required, and makes sure that only
	 * the last started worker process can bind its result, independently of the
	 * finish order.
	 */
	private static class AsyncDrawable extends BitmapDrawable
	{
		private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

		public AsyncDrawable(Resources res, Bitmap bitmap,
				BitmapWorkerTask bitmapWorkerTask)
		{
			super(res, bitmap);
			bitmapWorkerTaskReference = new WeakReference<BitmapWorkerTask>(
					bitmapWorkerTask);
		}

		public BitmapWorkerTask getBitmapWorkerTask()
		{
			return bitmapWorkerTaskReference.get();
		}
	}

	/**
	 * Called when the processing is complete and the final drawable should be
	 * set on the ImageView.
	 *
	 * @param imageView
	 * @param drawable
	 */
	private void setImageDrawable(IImage imageView, Drawable drawable)
	{

		imageView.Set(((BitmapDrawable) drawable).getBitmap(), true);

	}

	private void setImageDrawable(ImageView imageView, Drawable drawable)
	{

		imageView.setImageDrawable(drawable);
	}

	/**
	 * Pause any ongoing background work. This can be used as a temporary
	 * measure to improve performance. For example background work could be
	 * paused when a ListView or GridView is being scrolled using a
	 * {@link android.widget.AbsListView.OnScrollListener} to keep scrolling
	 * smooth.
	 * <p>
	 * If work is paused, be sure setPauseWork(false) is called again before
	 * your fragment or activity is destroyed (for example during
	 * {@link android.app.Activity#onPause()}), or there is a risk the
	 * background thread will never finish.
	 */
	public void setPauseWork(boolean pauseWork)
	{
		synchronized (mPauseWorkLock)
		{
			mPauseWork = pauseWork;
			if(!mPauseWork)
			{
				mPauseWorkLock.notifyAll();
			}
		}
	}


	/**
	 * Download a bitmap from a URL and write the content to an output stream.
	 *
	 * @param urlString
	 *            The URL to fetch
	 * @return true if successful, false otherwise
	 */
	public String downloadUrlToStream(String urlString,
			OutputStream outputStream)
	{
		Malevich.Utils.disableConnectionReuseIfNecessary();
		HttpURLConnection urlConnection = null;
		BufferedOutputStream out = null;
		BufferedInputStream in = null;
		String error = "";
		try
		{
			final URL url = new URL(urlString);
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setConnectTimeout(Utils.getConnectTimeout());
			urlConnection.setReadTimeout(Utils.getReadTimeout());
			urlConnection.setRequestProperty("User-Agent",
					Utils.getUserAgent());
			in = new BufferedInputStream(urlConnection.getInputStream(),
					IO_BUFFER_SIZE);
			out = new BufferedOutputStream(outputStream, IO_BUFFER_SIZE);

			int b;
			while ((b = in.read()) != -1)
			{
				out.write(b);
			}
			return error;
		} catch (final IOException e)
		{
			Log.e(TAG, "Error in downloadBitmap - " + e);
			error = "Error in downloadBitmap - " + e.toString();
		} finally
		{
			if(urlConnection != null)
			{
				urlConnection.disconnect();
			}
			try
			{
				if(out != null)
				{
					out.close();
				}
				if(in != null)
				{
					in.close();
				}
			} catch (final IOException e)
			{
			}
		}
		return error;
	}
}
