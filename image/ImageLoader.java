package com.utils.image;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.utils.App;
import com.utils.Utils;
import com.utils.io.IO;
import com.utils.ui.IImage;

public class ImageLoader
{


	public static Malevich MALEVICH;

	public static void initMalevichStd(Context context) {
	
		if(MALEVICH==null) {
			ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams();
			cacheParams.memoryCacheEnabled = true; // Enable memory cache
			cacheParams.setMemCacheSizePercent(0.4f); // Percent of available memory
														// for cache
			cacheParams.compressQuality = 70; // Compress quality
			cacheParams.compressFormat = CompressFormat.PNG; // Compress format

			
			MALEVICH = new Malevich.Builder(context).debug(false) 															
					.maxSize(1800) // max size of image in px
					.CacheParams(cacheParams) // custom cache
					.build();
		}
		
		
	}


	public static void LoadBackground(String uri)
	{

		if(uri == null)
		{
			return;
		}

		if(MALEVICH == null) {
			initMalevichStd(App.get());
		}
		
		MALEVICH.load(uri).intoNull();

	}

	public static void Load(IImage image, String uri,
			OnClickListener listener, boolean is_cached)
	{

		if(image == null || uri == null) {
			return;
		}

		if(MALEVICH == null) {
			initMalevichStd(image.getContext());
		}
		
		MALEVICH.load(uri).into(image);

		if(listener != null) {
			image.setOnClickListener(listener);
		}

	}

	public static void Load(ImageView image, String uri) {
		Load(image, uri, null, true);
	}
	
	public static void Load(IImage image, String uri) {
		Load(image, uri, null, true);
	}
	
	public static void Load(ImageView image, String uri,
			OnClickListener listener, boolean is_cached)
	{

		if(MALEVICH == null) {
			initMalevichStd(image.getContext());
		}

		MALEVICH.load(uri).into(image);

		if(listener != null)
		{
			image.setOnClickListener(listener);
		}
	}

	
	public static Bitmap removeBitmapFromMemCache(String name)
	{
		if(MALEVICH == null) {
			initMalevichStd(App.get());
		}
		
		MALEVICH.getImageCache().getMemoryCache().remove(name);
		
		return null;
	}
	
	public static Bitmap getBitmapFromMemCache(String name)
	{
		if(MALEVICH == null) {
			initMalevichStd(App.get());
		}
		
		BitmapDrawable bd = MALEVICH.getImageCache().getMemoryCache().get(name);
		
		return bd!=null&&bd.getBitmap()!=null&&!bd.getBitmap().isRecycled()?bd.getBitmap():removeBitmapFromMemCache(name);

	}
	
	@SuppressWarnings("deprecation")
	public static void addBitmapToMemoryCache(String name, Bitmap aResult)
	{
		if(MALEVICH == null) {
			initMalevichStd(App.get());
		}
		
		MALEVICH.getImageCache().getMemoryCache().put(name, new BitmapDrawable(aResult));
	}
	
	public static Bitmap GetAssetsImage(String name)
	{
		
		try 
		{
			
			final BufferedInputStream in = new BufferedInputStream(App.get().getAssets().open(name), IO.IO_BUFFER_SIZE);
			final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
			final BufferedOutputStream out = new BufferedOutputStream(dataStream, IO.IO_BUFFER_SIZE);
			IOUtils.copy(in, out);
			out.flush();
			
			byte[] mContent = dataStream.toByteArray();
			
			out.close();
			dataStream.close();
			in.close();

			return android.graphics.BitmapFactory.decodeByteArray(mContent, 0, mContent.length, Utils.getOptions());
			
		} catch (IOException e) {
			
		}

		return null;

	}
}
