package com.utils.image;

import java.util.ArrayList;

import com.utils.CanvasUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Movie;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class Gif extends View
{
	public Movie mMovie;
	
	private long mMovieStart;
	
	public static interface ISetMovie
	{
		void post(Movie m);
	}
	
	public void onAnimate()
	{
		
	}
	
	public void addCallback(ISetMovie callback)
	{
		callbacks.add(callback);
	}
	
	ArrayList<ISetMovie> callbacks = new ArrayList<ISetMovie>();
	

	public Gif(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	
		
		
		setBackgroundColor(Const.COLOR_PLACEHOLDER);
		
	}
	
	public Gif(Context context)
	{
		super(context);

		
		
		setBackgroundColor(Const.COLOR_PLACEHOLDER);
		
	}

	public void Set(Movie movie)
	{
		Set(movie, true);
	}
	boolean is_crop = true;
	public void Set(Movie movie, boolean is_crop)
	{
		
		this.is_crop = is_crop;
		
		for(ISetMovie callback: callbacks)
		{
			callback.post(movie);
		}
		
		
		rect_gif = null;
		rect_src = null;
		setTag("");
		k = 1f;
		is_need_redraw = true;
		mMovie = movie;
		
		canv = null;
		b = null;
		
		mMovieStart = 0;
		
		
		
		invalidate();
	}

	
	Bitmap b;
	Canvas canv;
	
	static Paint p_gif = new Paint();
	
	static
	{
		p_gif.setFilterBitmap(true);
		p_gif.setAntiAlias(true);
		p_gif.setDither(true);
		p_gif.setXfermode(new PorterDuffXfermode(Mode.SRC));
	}
	
	
	Rect rect_gif = null;
	Rect rect_src = null;
	float k = 1f;
	boolean is_need_redraw = true;
	
	@Override
	protected void onDetachedFromWindow()
	{
		
		super.onDetachedFromWindow();
		
		callbacks.clear();
	}
	@Override
	public void onDraw(Canvas canvas)
	{
		canvas.drawColor(Color.TRANSPARENT);
		
		super.onDraw(canvas);
		
		 
		long now = android.os.SystemClock.uptimeMillis();
		
		
		if(mMovie != null)
		{
			
			
			try
			{
				
				
				
				
				if(canv == null)
				{
					float max_w = 400;
					float max_h = 400;
					
					float k1 = max_w/mMovie.width();
					float k2 = max_h/mMovie.height();
					
					if(k1 < k)
					{
						k = k1;
					}
					
					if(k2 < k)
					{
						k = k2;
					}
					
					b = Bitmap.createBitmap((int)(mMovie.width() * k), (int) (mMovie.height() * k), Config.RGB_565);
					
					canv = new Canvas(b);
				}
				
				if(is_need_redraw)
				{
					is_need_redraw = false;
					
					if(mMovieStart == 0)
					{ 
						mMovieStart = now;
					}
					
					
					
					//b.eraseColor(Color.WHITE);
					
					int dur = mMovie.duration();
					
					if(dur == 0)
					{
						dur = 3000;
					}
					
					int relTime = (int) ((now - mMovieStart) % dur);
					
					mMovie.setTime(relTime);
					
					if(k!=1f)
					{
						canv.save();
						canv.scale(k, k);
					}

					mMovie.draw(canv, 0, 0,p_gif);
					
					if(k!=1f)
					{
						canv.restore();
					}
				}
				
				
				
				
				
				if(rect_gif==null)
				{
					rect_gif = CanvasUtils.DrawImage(canvas, b, canvas.getClipBounds(), is_crop, true, 255, true);
				}
				else
				{
					if(rect_src == null)
					{
						rect_src = new Rect(0, 0, b.getWidth(), b.getHeight());
					}
					
					CanvasUtils.DrawImage(canvas, b, canvas.getClipBounds(), rect_gif, rect_src, true, 255, true);
				}
				
			}
			catch (Exception e1)
			{
				
				e1.printStackTrace();
			}
			
			is_need_redraw = true;
			postInvalidateDelayed(40);
			/*
			new Handler().postDelayed(new Runnable()
			{
				
				@Override
				public void run()
				{
					try
					{
						is_need_redraw = true;
						invalidate();
					} catch (Exception e)
					{
						
					}
				}
			}, 40);
			*/
			
			
		}
		else
		{
			int w = getMeasuredWidth();
			int h = getMeasuredHeight();
			int wp = w / 3;
			
			Rect rect_img = new Rect(wp, 0, w - wp,	h );
			
			CanvasUtils.DrawImage(canvas, Const.PLACEHOLDER, rect_img, false, true, 255, false);
			
		}

	}

	public void Clear()
	{
		mMovie = null;
		
		setClickable(false);
	}

	public boolean isAnimating()
	{
		
		return true;
	}

	public boolean empty()
	{
		
		return mMovie == null;
	}
}