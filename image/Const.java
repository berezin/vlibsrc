package com.utils.image;

import android.graphics.Bitmap;

public class Const
{

	static public Bitmap PLACEHOLDER;
	
	static public float PLACEHOLDER_PADDING_PERCENT = 0.33f;

	static public boolean IS_DRAW_IMAGE_XFERMODESRC = true;

	static public Integer COLOR_PLACEHOLDER = 0xffeeeeee;

}
