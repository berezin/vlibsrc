package com.utils.image;

import java.io.File;
import java.util.HashSet;

import com.utils.io.IO;
import com.utils.setting.SettingHelper.Storage;
import com.utils.shorts.E;

import android.graphics.Bitmap;

public class ImageProcess
{
	public static final class Files extends HashSet<String>{

		/**
		 * 
		 */
		final private static long serialVersionUID = 4805145112170511896L;
		
		
		@Override
		final public String toString()
		{
			return String.valueOf(size());
		}
		
	}
	
	final static Storage<Files> files = new Storage<Files>(E.Id("Malevich.Files"), new Files()) {{
		setGZIP(true);
	}};
	final static Object lock = 0;
	
	final public static Bitmap Get(String url, int reqWidth, int reqHeight,	boolean onlyDownload)
	{
		
		Bitmap bitmap = null;
		String filename = IO.FileNameFromUrl("malevich2", url);
		File cache = new File(filename);
		
		Files set = files.Get();
		boolean hasFile = set.contains(filename);
		if(!hasFile) {
			
			if(IO.FileFromUrl(cache, url)!=null) {
				hasFile = true;
				set.add(filename);
				files.SaveDelayed(10000);
			}
		}
		
		if(!onlyDownload && hasFile) {
			
			synchronized (lock)
			{
				try
				{
					bitmap = Malevich.Utils.decodeSampledBitmapFromFile(
							filename, reqWidth, reqHeight, ImageCache.Self());
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		
		return bitmap;
	}
}
