package com.utils;

import com.utils.shorts.Q;
import java.lang.reflect.Field;

/**
 * Created by berezin-ds on 02.06.2015.
 */
public class Dimen {

    public static Integer d1;
    public static Integer d2;
    public static Integer d3;
    public static Integer d4;
    public static Integer d5;
    public static Integer d6;
    public static Integer d7;
    public static Integer d8;
    public static Integer d9;

    public static Integer d10;
    public static Integer d11;
    public static Integer d12;
    public static Integer d13;
    public static Integer d14;
    public static Integer d15;
    public static Integer d16;
    public static Integer d17;
    public static Integer d18;
    public static Integer d19;

    public static Integer d20;
    public static Integer d21;
    public static Integer d22;
    public static Integer d23;
    public static Integer d24;
    public static Integer d25;
    public static Integer d26;
    public static Integer d27;
    public static Integer d28;
    public static Integer d29;

    public static Integer d30;
    public static Integer d31;
    public static Integer d32;
    public static Integer d33;
    public static Integer d34;
    public static Integer d35;
    public static Integer d36;
    public static Integer d37;
    public static Integer d38;
    public static Integer d39;

    public static Integer d40;
    public static Integer d41;
    public static Integer d42;
    public static Integer d43;
    public static Integer d44;
    public static Integer d45;
    public static Integer d46;
    public static Integer d47;
    public static Integer d48;
    public static Integer d49;

    public static Integer d50;
    public static Integer d55;
    public static Integer d60;
    public static Integer d64;
    public static Integer d65;
    public static Integer d70;
    public static Integer d75;
    public static Integer d80;
    public static Integer d85;
    public static Integer d90;
    public static Integer d95;
    public static Integer d100;
    public static Integer d105;
    public static Integer d110;
    public static Integer d115;
    public static Integer d120;
    public static Integer d125;
    public static Integer d130;
    public static Integer d135;
    public static Integer d140;
    public static Integer d145;
    public static Integer d150;
    public static Integer d160;
    public static Integer d170;

    public static Integer d200;
    public static Integer d250;
    public static Integer d300;
    public static Integer d310;
    public static Integer d320;
    public static Integer d330;
    public static Integer d340;
    public static Integer d350;
    public static Integer d400;
    public static Integer d450;

    public static Float f1;
    public static Float f2;
    public static Float f3;
    public static Float f4;
    public static Float f5;
    public static Float f6;
    public static Float f7;
    public static Float f8;
    public static Float f9;
    public static Float f10;

    public static Float f11;
    public static Float f12;
    public static Float f13;
    public static Float f14;
    public static Float f15;
    public static Float f16;
    public static Float f17;
    public static Float f18;
    public static Float f19;
    public static Float f20;
	

    static{


        Field[] declaredFields = Dimen.class.getDeclaredFields();

        for (Field field : declaredFields) {

            try
            {
                String name = field.getName();

                if(name.charAt(0) == 'd')
                {
                    field.set(null, Q.getRealSizeInt(Integer.parseInt(name.substring(1))));
                }
                else  if(name.charAt(0) == 'f')
                {
                    field.set(null, Q.getRealSize(Float.parseFloat(name.substring(1))));
                }


            }
            catch (Exception e) {
                e.printStackTrace();
            }

        }


    }

}
